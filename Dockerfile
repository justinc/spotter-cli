ARG BASE_IMAGE_PREFIX=""
FROM ${BASE_IMAGE_PREFIX}library/python:3.12.1-alpine3.19

WORKDIR /scan
ENTRYPOINT ["spotter"]

RUN --mount=type=bind,source=.,target=/repodir/ \
    test "$(ls -l /repodir/*.whl | wc -l)" -eq 1 || { echo "None or more than one wheel present"; exit 2; } \
    && apk --no-cache add gcc musl-dev \
    && pip install --no-cache-dir /repodir/*.whl \
    && apk del gcc musl-dev

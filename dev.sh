#!/bin/bash

set -eu

run_linters() {
    black --config pyproject.toml --check --diff .
    pylint --rcfile pyproject.toml src/ tests/
    mypy --junit-xml mypy-report.xml --config-file pyproject.toml src/ tests/
    bandit --configfile pyproject.toml --recursive src/ --format xml --output bandit-report.xml
}

run_formatters() {
    black --config pyproject.toml .
}

run_unit_tests() {
    pytest tests/unit/ --junit-xml pytest-report.xml
}

run_integration() {
    for dir in tests/integration/*/; do (cd "$dir" && ./runme.sh spotter); done
}

run_help() {
    cat <<EOF
usage:
    ./dev.sh <command>

commands:
    lint                runs linters
    format              runs formatters
    unit                runs unit tests
    integration         runs integration tests
    help                shows this help
EOF
}

if [ $# -ne 1 ]; then
    echo -e "No arguments were supplied\n"
    run_help
    exit 1
fi

command="$1"
shift

case "$command" in
lint)
    run_linters
    ;;
format)
    run_formatters
    ;;
unit)
    run_unit_tests
    ;;
integration)
    run_integration
    ;;
help)
    run_help
    ;;
*)
    echo -e "Invalid command: $command\n"
    run_help
    ;;
esac

#!/bin/bash

set -eux

if [ -z "${RUN_ENV}" ]; then
    echo "RUN_ENV is not set"
    exit 1
fi

main() {
    eval "$(pyenv init -)"
    echo "RUN_ENV: $RUN_ENV"
    python_v=$(echo $RUN_ENV | grep -oE 'py([0-9]+\.[0-9]+\.[0-9]+)' | grep -oE '([0-9]+\.[0-9]+\.[0-9]+)')
    ansi_v=$(echo $RUN_ENV | grep -oE 'ansi([0-9]+\.[0-9]+\.[0-9]+)' | grep -oE '([0-9]+\.[0-9]+\.[0-9]+)') 
    pydantic_v=$(echo "$RUN_ENV" | grep -oE "dantic([0-9]+\.[0-9]+\.[0-9]+)" | grep -oE "([0-9]+\.[0-9]+\.[0-9]+)")

    echo "py${python_v}-ansi${ansi_v}"
    pyenv activate "py${python_v}-ansi${ansi_v}-dantic-${pydantic_v}"

    # this is how GitLab expects your entrypoint to end, if provided
    # will execute scripts from stdin
    exec /bin/bash
}

main

\x1b[1mRewritten check results\x1b[22m:
/home/user/Desktop/examples/playbook.yml:5:7: HINT: [H1900] Required collection \x1b[1msensu.sensu_go\x1b[22m is missing from requirements.yml or requirements.yml is missing.

\x1b[1mRemaining check results\x1b[22m:
\x1b[31m/home/user/Desktop/examples/playbook.yml:5:7: ERROR: [E005]\x1b[39m \x1b[1m\x1b[31mname\x1b[39m\x1b[22m is a required parameter in module \x1b[1m\x1b[31msensu.sensu_go.user\x1b[39m\x1b[22m. View docs at \x1b[36mhttps://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html\x1b[39m for more info.

\x1b[1mScan summary\x1b[22m:
Spotter took \x1b[1m0.298 s\x1b[22m to scan your input.
It resulted in \x1b[1m\x1b[31m1 error(s)\x1b[39m\x1b[22m, \x1b[1m\x1b[33m1 warning(s)\x1b[39m\x1b[22m and \x1b[1m2 hint(s)\x1b[22m.
Can \x1b[1m\x1b[35mrewrite\x1b[39m\x1b[22m \x1b[1m0 file(s)\x1b[22m with \x1b[1m0 change(s)\x1b[22m.
Overall status: \x1b[1m\x1b[31mERROR\x1b[39m\x1b[22m
Visit \x1b[36mhttps://spotter.xlab.si/organization/org_url/projects/proj_url/scans/scan_url/\x1b[39m to view this scan result.
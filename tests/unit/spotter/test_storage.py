import json
from pathlib import Path

from spotter.library.storage import Storage


class TestStorage:
    def test_create(self, tmp_path: Path) -> None:
        Storage(tmp_path / "storage")

        assert (tmp_path / "storage").exists() is True

    def test_create_not_dir(self, tmp_path: Path) -> None:
        Storage(tmp_path / "storage.txt")

        assert (tmp_path / "storage").exists() is False

    def test_write(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        content = "Hello from example!"
        storage.write(content, "write.txt")

        assert (storage.path / "write.txt").exists() is True
        with (storage.path / "write.txt").open("r", encoding="utf-8") as f:
            assert f.read() == content

    def test_write_json(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        content = {"hello": "world", "hi": 123, "true": False, "lst": ["a", "b", "c", "d"]}
        storage.write_json(content, "write.json")

        assert (storage.path / "write.json").exists() is True
        with (storage.path / "write.json").open("r", encoding="utf-8") as f:
            assert json.loads(f.read()) == content

    def test_update_json(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        (storage.path / "write.json").write_text(json.dumps({"hello": "world", "hi": 123}))

        storage.update_json({"hello": "moon", "false": True}, "write.json")

        assert (storage.path / "write.json").exists() is True
        with (storage.path / "write.json").open("r", encoding="utf-8") as f:
            assert json.loads(f.read()) == {"hello": "moon", "hi": 123, "false": True}

    def test_read(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        read_path = storage.path / "read.txt"
        content = "Hello from example!"
        read_path.write_text(content)

        assert storage.read(str(read_path)) == content

    def test_read_json(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        read_path = storage.path / "read.json"
        content = {"hello": "world", "hi": 123, "true": False, "lst": ["a", "b", "c", "d"]}
        read_path.write_text(json.dumps(content))

        assert storage.read_json(str(read_path)) == content

    def test_exists(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        existing_path = storage.path / "existing.txt"
        existing_path.write_text("This file exists!")

        assert storage.exists(str(existing_path)) is True

    def test_not_exists(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        nonexistent_path = storage.path / "nonexistent.txt"

        assert storage.exists(str(nonexistent_path)) is False

    def test_remove(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        path_to_remove = storage.path / "remove.txt"
        path_to_remove.write_text("This file will be removed!")
        storage.remove(str(path_to_remove))

        assert path_to_remove.exists() is False

    def test_remove_all(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        path_to_remove1 = storage.path / "remove.txt"
        path_to_remove1.write_text("This file will be removed!")
        (storage.path / "remove").mkdir()
        path_to_remove2 = storage.path / "remove" / "remove.txt"
        path_to_remove2.write_text("This file will be removed too!")
        path_to_remove3 = storage.path / "remove_dir"
        path_to_remove3.mkdir()
        storage.remove_all()

        assert (tmp_path / "storage").exists() is False
        assert path_to_remove1.exists() is False
        assert path_to_remove2.exists() is False
        assert path_to_remove3.exists() is False

    def test_remove_all_keep_root_dir(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        path_to_remove1 = storage.path / "remove.txt"
        path_to_remove1.write_text("This file will be removed!")
        (storage.path / "remove").mkdir()
        path_to_remove2 = storage.path / "remove" / "remove.txt"
        path_to_remove2.write_text("This file will be removed too!")
        path_to_remove3 = storage.path / "remove_dir"
        path_to_remove3.mkdir()
        storage.remove_all(keep_root_dir=True)

        assert (tmp_path / "storage").exists() is True
        assert path_to_remove1.exists() is False
        assert path_to_remove2.exists() is False
        assert path_to_remove3.exists() is False

    def test_copy_file(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        original_path = storage.path / "original.txt"
        original_path.write_text("This file exists!")
        storage.copy_file("original.txt", "copy.txt")

        assert (storage.path / "original.txt").exists() is True
        assert (storage.path / "copy.txt").exists() is True

    def test_move_file(self, tmp_path: Path) -> None:
        storage = Storage(tmp_path / "storage")
        original_path = storage.path / "original.txt"
        original_path.write_text("This file exists!")
        storage.move_file("original.txt", "moved.txt")

        assert (storage.path / "original.txt").exists() is False
        assert (storage.path / "moved.txt").exists() is True

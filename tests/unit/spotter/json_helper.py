"""
Value NaN is used instead of ANY inside JSON files, since we need ANY functionality.

It was easiest to implement like that. Otherwise we would need to update also lexer
to introduce new keyword. Since we do not use float math at all, we just use not
used constant NaN, and threat it like ANY.
"""
import json
from json import JSONEncoder
from typing import Any
from unittest.mock import ANY, _ANY

import ruamel.yaml as ruamel

from spotter.library.parsing.parsing import SafeLineConstructor

CONSTANT_MAPPER = {"NaN": ANY}.__getitem__


class JSONEncoderAny(JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, _ANY):
            return JSONEncoder.default(self, float("nan"))
        return JSONEncoder.default(self, o)


def load_json(filename: str) -> Any:
    with open(filename, "r", encoding="utf-8") as f:
        result = json.load(f, parse_constant=CONSTANT_MAPPER)
        return result


def dump_json(obj: Any, filename: str) -> None:
    with open(filename, "w", encoding="utf-8") as f:
        json.dump(obj, f, cls=JSONEncoderAny)


def load_yaml(filename: str) -> Any:
    with open(filename, "r", encoding="utf-8") as f:
        yaml = ruamel.YAML(typ="rt")
        yaml.Constructor = SafeLineConstructor
        yaml.version = (1, 1)
        result = yaml.load(f)
        return result

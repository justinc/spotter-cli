from io import StringIO
from pathlib import Path
from typing import Any

import pytest
import ruamel.yaml as ruamel

from spotter.library.parsing.parsing import SafeLineConstructor
from spotter.library.rewriting.models import CheckType, RewriteSuggestion, RewriteResult, INodeSuggestion
from spotter.library.rewriting.processor import RewriteProcessor, update_files
from spotter.library.rewriting.rewrite_action_inline import RewriteActionInline
from spotter.library.rewriting.rewrite_action_object import RewriteActionObject
from spotter.library.rewriting.rewrite_always_run import RewriteAlwaysRun
from spotter.library.rewriting.rewrite_fqcn import RewriteFqcn
from spotter.library.rewriting.rewrite_inline import RewriteInline
from spotter.library.rewriting.rewrite_local_action_inline import RewriteLocalActionInline
from spotter.library.rewriting.rewrite_local_object import RewriteLocalActionObject
from spotter.library.rewriting.rewrite_module_inline import RewriteModuleInline
from spotter.library.rewriting.rewrite_module_object import RewriteModuleObject
from spotter.library.rewriting.rewrite_requirements import update_requirements
from spotter.library.scanning.display_level import DisplayLevel


# pylint: disable=too-many-public-methods
class TestRewriting:
    @pytest.fixture()
    def ansible_playbook_before_rewrite(self) -> list[dict[str, Any]]:
        return [
            {
                "name": "OpenSSL example",
                "hosts": "localhost",
                "tasks": [
                    {
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "openssl_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    }
                ],
            }
        ]

    @pytest.fixture()
    def ansible_playbook_after_rewrite(self) -> list[dict[str, Any]]:
        return [
            {
                "name": "OpenSSL example",
                "hosts": "localhost",
                "tasks": [
                    {
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "community.crypto.x509_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    }
                ],
            }
        ]

    def test_create_check_type_from_string(self) -> None:
        check_type = CheckType.from_string("task")

        assert check_type.name == "TASK"
        assert check_type.value == "task"

    def test_create_check_type_from_string_nonexistent_type(self) -> None:
        check_type = CheckType.from_string("my_type")

        assert check_type.name == "OTHER"
        assert check_type.value == "other"

    def test_suggestion_from_task(self) -> None:
        task = {
            "task_id": "c9d23f56-03d5-4c11-a935-c78d33e22936",
            "task_args": {
                "name": "Ensure that the server certificate belongs to the specified private key",
                "openssl_certificate": {
                    "path": "{{ config_path }}/certificates/server.crt",
                    "privatekey_path": "{{ config_path }}/certificates/server.key",
                    "provider": "assertonly",
                },
            },
            "spotter_metadata": {
                "file": "/tmp/playbook.yaml",
                "line": 5,
                "column": 7,
                "start_mark_index": 64,
                "end_mark_index": 325,
            },
        }
        suggestion = {
            "data": {
                "after": "community.crypto.x509_certificate",
                "before": "openssl_certificate",
            },
            "action": "FIX_FQCN",
        }

        assert RewriteSuggestion.from_item(CheckType.TASK, task, suggestion, DisplayLevel.HINT) == RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={
                "name": "Ensure that the server certificate belongs to the specified private key",
                "openssl_certificate": {
                    "path": "{{ config_path }}/certificates/server.crt",
                    "privatekey_path": "{{ config_path }}/certificates/server.key",
                    "provider": "assertonly",
                },
            },
            file=Path("/tmp/playbook.yaml"),
            file_parent=Path("/tmp"),
            line=5,
            column=7,
            start_mark=64,
            end_mark=325,
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
            display_level=DisplayLevel.HINT,
        )

    def test_update_content(
        self,
        ansible_playbook_before_rewrite: list[dict[str, Any]],
        ansible_playbook_after_rewrite: list[dict[str, Any]],
    ) -> None:
        yaml = ruamel.YAML(typ="rt")
        content_before = StringIO()
        yaml.dump(ansible_playbook_before_rewrite, content_before)
        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={
                "name": "Ensure that the server certificate belongs to the specified private key",
                "openssl_certificate": {
                    "path": "{{ config_path }}/certificates/server.crt",
                    "privatekey_path": "{{ config_path }}/certificates/server.key",
                    "provider": "assertonly",
                },
            },
            file=Path("/tmp/playbook.yml"),
            file_parent=Path("/tmp"),
            line=0,
            column=0,
            start_mark=64,
            end_mark=325,
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
            display_level=DisplayLevel.HINT,
        )

        content_after = StringIO()
        yaml.dump(ansible_playbook_after_rewrite, content_after)
        # pylint: disable=protected-access
        assert RewriteProcessor.execute(content_before.getvalue(), suggestion) == RewriteResult(
            content=content_after.getvalue(),
            diff_size=14,
        )

    def test_update_files(
        self,
        tmp_path: Path,
        ansible_playbook_before_rewrite: list[dict[str, Any]],
        ansible_playbook_after_rewrite: list[dict[str, Any]],
    ) -> None:
        playbook_path = tmp_path / "playbook.yml"
        yaml = ruamel.YAML(typ="rt")
        yaml.dump(ansible_playbook_before_rewrite, playbook_path)

        update_files(
            [
                RewriteSuggestion(
                    check_type=CheckType.TASK,
                    item_args={
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "openssl_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    },
                    file=playbook_path,
                    file_parent=tmp_path,
                    line=0,
                    column=0,
                    start_mark=64,
                    end_mark=325,
                    suggestion_spec={
                        "data": {"after": "community.crypto.x509_certificate", "before": "openssl_certificate"},
                        "action": "FIX_FQCN",
                    },
                    display_level=DisplayLevel.HINT,
                )
            ],
            display_level=DisplayLevel.HINT,
        )

        with playbook_path.open("r", encoding="utf-8") as f:
            playbook_rewritten = StringIO()
            yaml.dump(ruamel.YAML(typ="safe").load(f), playbook_rewritten)

        playbook_after_rewrite = StringIO()
        yaml.dump(ansible_playbook_after_rewrite, playbook_after_rewrite)
        assert playbook_rewritten.getvalue() == playbook_after_rewrite.getvalue()

    def test_fqcn_apply(self) -> None:
        with open("tests/unit/data/rewriting/input/fqcn.yml", "r", encoding="utf-8") as f:
            content = f.read()
        with open("tests/unit/data/rewriting/output/fqcn.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=0,
            column=0,
            start_mark=6,
            end_mark=len(content),
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteFqcn()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert rewrite_result.content == expected
        assert rewrite_result.diff_size == 14

    def test_fqcn_diff(self) -> None:
        with open("tests/unit/data/rewriting/input/fqcn.yml", "r", encoding="utf-8") as f:
            content = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=0,
            column=0,
            start_mark=6,
            end_mark=len(content),
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteFqcn()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        diff_minus, diff_plus = replacement.get_diff()
        assert diff_minus == "    openssl_certificate:"
        assert diff_plus == "    community.crypto.x509_certificate:"

    def test_inline_apply(self) -> None:
        with open("tests/unit/data/rewriting/input/inline.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/inline.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_INLINE",
                "data": {
                    "args": {"file": "my_test_tasks.yml"},
                    "vars": {"a": "b", "c": "d"},
                    "module_name": "include_tasks",
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert rewrite_result.diff_size > 0
        assert rewrite_result.content == expected

        with open("tests/unit/data/rewriting/input/inline2.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/inline2.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_INLINE",
                "data": {
                    "args": {"file": "my_test_tasks.yml"},
                    "vars": {"a": "b", "c": "d"},
                    "module_name": "include_tasks",
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert rewrite_result.diff_size > 0
        assert rewrite_result.content == expected

    def test_always_run_apply(self) -> None:
        with open("tests/unit/data/rewriting/input/always_run.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/always_run.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={"action": "FIX_ALWAYS_RUN", "data": {"before": "always_run"}},
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteAlwaysRun()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_action_inlined(self) -> None:
        with open("tests/unit/data/rewriting/input/action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/action_inlined.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_ACTION_INLINE",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteActionInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_local_action_object(self) -> None:
        with open("tests/unit/data/rewriting/input/local_action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/local_action_object.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_LOCAL_ACTION_OBJECT",
                "data": {
                    "original_module_name": "local_action",
                    "module_name": "file",
                    "additional": [{"delegate_to": "localhost"}],
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteLocalActionObject()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_local_action_inlined(self) -> None:
        with open("tests/unit/data/rewriting/input/local_action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/local_action_inlined.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=len(content),
            suggestion_spec={
                "action": "FIX_LOCAL_ACTION_INLINE",
                "data": {
                    "original_module_name": "local_action",
                    "module_name": "file",
                    "additional": [{"delegate_to": "localhost"}],
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteLocalActionInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_action_object(self) -> None:
        with open("tests/unit/data/rewriting/input/action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/action_object.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_ACTION_OBJECT",
                "data": {"original_module_name": "action", "module_name": "file", "additional": []},
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteActionObject()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_module_inline(self) -> None:
        with open("tests/unit/data/rewriting/input/action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
        with open("tests/unit/data/rewriting/output/module_inline.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=0,
            column=0,
            start_mark=0,
            end_mark=len(content),
            suggestion_spec={
                "action": "FIX_ACTION_INLINE",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteModuleInline(suggestion)
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_module_object(self) -> None:
        with open("tests/unit/data/rewriting/input/action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/module_object.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            line=as_yml[0]["__meta__"]["__line__"],
            column=as_yml[0]["__meta__"]["__column__"],
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_ACTION_OBJECT",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                },
            },
            display_level=DisplayLevel.HINT,
        )

        rewriter = RewriteModuleObject()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_requirements_create(self, tmp_path: Path) -> None:
        output_requirements_path = Path("tests/unit/data/rewriting/output/requirements.yml")
        tmp_requirements_path = tmp_path / "requirements.yml"
        suggestion1 = RewriteSuggestion(
            check_type=CheckType.REQUIREMENTS,
            item_args={},
            file=tmp_requirements_path,
            file_parent=tmp_requirements_path.parent,
            line=0,
            column=0,
            start_mark=0,
            end_mark=0,
            suggestion_spec={
                "data": {"version": "2.11.1", "collection_name": "community.crypto"},
                "action": "FIX_REQUIREMENTS",
            },
            display_level=DisplayLevel.ERROR,
        )
        suggestion2 = RewriteSuggestion(
            check_type=CheckType.REQUIREMENTS,
            item_args={},
            file=tmp_requirements_path,
            file_parent=tmp_requirements_path.parent,
            line=0,
            column=0,
            start_mark=0,
            end_mark=0,
            suggestion_spec={
                "data": {"version": "1.13.2", "collection_name": "sensu.sensu_go"},
                "action": "FIX_REQUIREMENTS",
            },
            display_level=DisplayLevel.ERROR,
        )

        assert tmp_requirements_path.exists() is False
        suggestions = INodeSuggestion(tmp_requirements_path, iter([suggestion1, suggestion2]))
        update_requirements([suggestions], DisplayLevel.SUCCESS)
        assert tmp_requirements_path.exists() is True

        yaml = ruamel.YAML(typ="rt")
        print(output_requirements_path.read_text(encoding="utf-8"))
        print(tmp_requirements_path.read_text(encoding="utf-8"))
        output_as_dict = dict(yaml.load(output_requirements_path.read_text(encoding="utf-8")))
        tmp_as_dict = dict(yaml.load(tmp_requirements_path.read_text(encoding="utf-8")))

        assert len(tmp_as_dict["collections"]) == 2
        assert tmp_as_dict == output_as_dict

    def test_requirements_append(self, tmp_path: Path) -> None:
        input_requirements_path = Path("tests/unit/data/rewriting/input/requirements.yml")
        output_requirements_path = Path("tests/unit/data/rewriting/output/requirements.yml")
        tmp_requirements_path = tmp_path / "requirements.yml"
        suggestion = RewriteSuggestion(
            check_type=CheckType.REQUIREMENTS,
            item_args={},
            file=tmp_requirements_path,
            file_parent=tmp_requirements_path.parent,
            line=0,
            column=0,
            start_mark=0,
            end_mark=0,
            suggestion_spec={
                "data": {"version": "1.13.2", "collection_name": "sensu.sensu_go"},
                "action": "FIX_REQUIREMENTS",
            },
            display_level=DisplayLevel.ERROR,
        )

        input_content = input_requirements_path.read_text(encoding="utf-8")
        assert tmp_requirements_path.exists() is False
        tmp_requirements_path.write_text(input_content)
        assert tmp_requirements_path.exists() is True

        yaml = ruamel.YAML(typ="rt")
        tmp_as_dict = dict(yaml.load(tmp_requirements_path.read_text(encoding="utf-8")))

        assert len(tmp_as_dict["collections"]) == 1

        suggestions = INodeSuggestion(tmp_requirements_path, iter([suggestion]))
        update_requirements([suggestions], DisplayLevel.SUCCESS)

        output_as_dict = dict(yaml.load(output_requirements_path.read_text(encoding="utf-8")))
        tmp_as_dict = dict(yaml.load(tmp_requirements_path.read_text(encoding="utf-8")))

        assert len(tmp_as_dict["collections"]) == 2
        assert tmp_as_dict == output_as_dict

    def test_multiexecute_action_inlined(self) -> None:
        with open("tests/unit/data/rewriting/input/action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_ACTION_INLINE",
                    "data": {
                        "original_module_name": "action",
                        "module_name": "file",
                        "additional": [{"delegate_to": "localhost"}],
                    },
                },
                display_level=DisplayLevel.HINT,
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_INLINE",
                    "data": {"args": {"mode": "0644", "state": "present"}, "module_name": "file"},
                },
                display_level=DisplayLevel.HINT,
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
                display_level=DisplayLevel.HINT,
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion, display_level=DisplayLevel.HINT)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_local_action_inlined(self) -> None:
        with open("tests/unit/data/rewriting/input/local_action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_LOCAL_ACTION_INLINE",
                    "data": {
                        "original_module_name": "local_action",
                        "module_name": "file",
                        "additional": [{"delegate_to": "localhost"}],
                    },
                },
                display_level=DisplayLevel.HINT,
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_INLINE",
                    "data": {"args": {"mode": "0644", "state": "present"}, "module_name": "file"},
                },
                display_level=DisplayLevel.HINT,
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
                display_level=DisplayLevel.HINT,
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion, display_level=DisplayLevel.HINT)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_action_object(self) -> None:
        with open("tests/unit/data/rewriting/input/action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_ACTION_OBJECT",
                    "data": {"original_module_name": "action", "module_name": "file", "additional": []},
                },
                display_level=DisplayLevel.HINT,
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
                display_level=DisplayLevel.HINT,
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion, display_level=DisplayLevel.HINT)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_local_action_object(self) -> None:
        with open("tests/unit/data/rewriting/input/local_action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            yaml = ruamel.YAML(typ="rt")
            yaml.Constructor = SafeLineConstructor
            yaml.version = (1, 1)
            as_yml = yaml.load(f)
        with open("tests/unit/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_LOCAL_ACTION_OBJECT",
                    "data": {
                        "original_module_name": "local_action",
                        "module_name": "file",
                        "additional": [{"delegate_to": "localhost"}],
                    },
                },
                display_level=DisplayLevel.HINT,
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                line=as_yml[0]["__meta__"]["__line__"],
                column=as_yml[0]["__meta__"]["__column__"],
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
                display_level=DisplayLevel.HINT,
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion, display_level=DisplayLevel.HINT)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

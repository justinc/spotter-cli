import argparse
import sys
from pathlib import Path
from typing import Callable
from unittest import mock
import tempfile
import pytest

from spotter.library.utils import get_package_version, get_current_cli_version, validate_url, get_relative_path_to_cwd

if sys.version_info >= (3, 8):
    from importlib.metadata import Distribution, PathDistribution
else:
    from pkg_resources import Distribution


def _mock_distribution(name: str, version: str, tmp_path: Path) -> Distribution:
    if sys.version_info >= (3, 8):
        metadata_path = tmp_path / "METADATA"
        metadata_path.write_text(f"Metadata-Version: 2.1\nName: {name}\nVersion: {version}")
        distribution = PathDistribution(metadata_path)
        return distribution
    return Distribution(version=version)


with tempfile.TemporaryDirectory() as temp_dir:
    with mock.patch(
        "spotter.library.utils.get_distribution",
        return_value=_mock_distribution("steampunk-spotter", "3.2.2.dev12+gbd2cad1", Path(temp_dir)),
    ):
        from spotter.client.utils import CommonParameters, get_absolute_path  # pylint: disable=ungrouped-imports


class TestUtils:
    @pytest.fixture()
    def mock_distribution(self) -> Callable[[str, str, Path], Distribution]:
        return _mock_distribution

    def test_get_package_version_existing(
        self, tmp_path: Path, mock_distribution: Callable[[str, str, Path], Distribution]
    ) -> None:
        with mock.patch(
            "spotter.library.utils.get_distribution", return_value=mock_distribution("package", "1.2.3", tmp_path)
        ):
            assert get_package_version("package", False) == "1.2.3"

    def test_get_package_version_nonexistent_no_throw(self) -> None:
        assert get_package_version("package123456789", False) is None

    def test_get_package_version_nonexistent_throw(self) -> None:
        with pytest.raises(SystemExit) as exception_info:
            get_package_version("package123456789", True)

        assert exception_info.value.code == 2

    def test_get_current_cli_version(
        self, tmp_path: Path, mock_distribution: Callable[[str, str, Path], Distribution]
    ) -> None:
        with mock.patch(
            "spotter.library.utils.get_distribution",
            return_value=mock_distribution("steampunk-spotter", "2.2.2.dev12+gbd2cad1", tmp_path),
        ):
            assert get_current_cli_version() == "2.2.2.dev12+gbd2cad1"

    @pytest.mark.parametrize("url", ["http://api.spotter.si/api", "https://api.spotter.si/api"])
    def test_validate_url(self, url: str) -> None:
        validate_url(url)

    def test_validate_url_invalid_scheme(self) -> None:
        url = "file://api.spotter.si/api"
        with pytest.raises(argparse.ArgumentTypeError) as exception_info:
            validate_url("file://api.spotter.si/api")

        assert str(exception_info.value) == f"URL '{url}' has an invalid URL scheme 'file', supported are: http, https."

    def test_validate_url_invalid_length(self) -> None:
        url = f"https://{'a' * 2048}"
        with pytest.raises(argparse.ArgumentTypeError) as exception_info:
            validate_url(url)

        assert str(exception_info.value) == f"URL '{url}' exceeds maximum length of 2048 characters."

    def test_validate_url_invalid_domain(self) -> None:
        url = "https://"
        with pytest.raises(argparse.ArgumentTypeError) as exception_info:
            validate_url(url)

        assert str(exception_info.value) == f"No URL domain specified in '{url}'."

    def test_get_relative_path_to_cwd(self) -> None:
        cwd = "/home/user/Desktop/steampunk-spotter/spotter-cli"
        absolute_path = f"{cwd}/playbooks/playbook.yml"
        with mock.patch.object(Path, "cwd", return_value=cwd):
            relative_path = get_relative_path_to_cwd(absolute_path)

        assert relative_path == "playbooks/playbook.yml"

    def test_get_relative_path_to_cwd_uncommon_path(self) -> None:
        cwd = "/home/user/Desktop/steampunk-spotter/spotter-cli"
        absolute_path = "/home/user/Downloads/playbooks/playbook.yml"
        with mock.patch.object(Path, "cwd", return_value=cwd):
            relative_path = get_relative_path_to_cwd(absolute_path)

        assert relative_path == absolute_path

    def test_create_common_parameters(self, tmp_path: Path) -> None:
        common_parameters = CommonParameters(
            api_endpoint="https://api.spotter.steampunk.si/api",
            storage_path=(tmp_path / "storage"),
            api_token="token",
            username=None,
            password=None,
            timeout=None,
        )

        assert common_parameters.api_endpoint == "https://api.spotter.steampunk.si/api"
        assert common_parameters.storage_path == tmp_path / "storage"
        assert common_parameters.api_token == "token"
        assert common_parameters.username is None
        assert common_parameters.password is None
        assert common_parameters.timeout is None
        assert common_parameters.debug is False

    def test_from_args(self, tmp_path: Path) -> None:
        args = argparse.Namespace(
            endpoint="https://api.spotter.steampunk.si/api",
            storage_path=(tmp_path / "storage"),
            token=None,
            username="user",
            password="password",
            timeout=60,
            debug=True,
        )
        common_parameters = CommonParameters.from_args(args)

        assert common_parameters.api_endpoint == "https://api.spotter.steampunk.si/api"
        assert common_parameters.storage_path == tmp_path / "storage"
        assert common_parameters.api_token is None
        assert common_parameters.username == "user"
        assert common_parameters.password == "password"
        assert common_parameters.timeout == 60
        assert common_parameters.debug is True

    def test_get_absolute_path(self, tmp_path: Path) -> None:
        path = tmp_path / "file.txt"
        path.touch()
        absolute_path = get_absolute_path(str(path))

        assert absolute_path == path.absolute()

    def test_get_absolute_path_nonexistent(self, tmp_path: Path) -> None:
        path = tmp_path / "nonexistent.txt"

        with pytest.raises(SystemExit) as exception_info:
            get_absolute_path(str(path))

        assert exception_info.value.code == 2

    def test_get_absolute_path_no_check_exists(self, tmp_path: Path) -> None:
        path = tmp_path / "nonexistent.txt"
        absolute_path = get_absolute_path(str(path), False)

        assert absolute_path == path.absolute()

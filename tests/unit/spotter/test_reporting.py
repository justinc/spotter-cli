import json
import sys
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import Callable
from unittest import mock

import pytest

from spotter.library.reporting.report import JUnitXml, SarifReport
from spotter.library.rewriting.models import CheckType
from spotter.library.scanning.check_catalog_info import CheckCatalogInfo
from spotter.library.scanning.check_result import CheckResult
from spotter.library.scanning.display_level import DisplayLevel
from spotter.library.scanning.item_metadata import ItemMetadata
from tests.unit.spotter.test_utils import _mock_distribution

if sys.version_info >= (3, 8):
    from importlib.metadata import Distribution
else:
    from pkg_resources import Distribution


class TestReporting:
    @pytest.fixture()
    def mock_distribution(self) -> Callable[[str, str, Path], Distribution]:
        return _mock_distribution

    def test_junit_xml_add_root_node(self) -> None:
        junit_renderer = JUnitXml()
        root = junit_renderer.add_root_node()

        assert isinstance(root, ET.Element)
        assert root.tag == "testsuites"

    def test_junit_xml_add_test_suite(self) -> None:
        root = ET.Element("testsuites")
        junit_renderer = JUnitXml()
        test_suite = junit_renderer.add_test_suite(root, "CheckParameters")

        assert isinstance(test_suite, ET.Element)
        assert test_suite.tag == "testsuite"
        assert test_suite.attrib == {"name": "CheckParameters"}

    def test_junit_xml_add_test_case(self) -> None:
        root = ET.Element("testsuites")
        test_suite = ET.SubElement(root, "testsuite", name="CheckParameters")
        junit_renderer = JUnitXml()
        test_case = junit_renderer.add_test_case(test_suite, "E005-required-parameter-missing[0]", "CheckParameters")

        assert isinstance(test_case, ET.Element)
        assert test_case.tag == "testcase"
        assert test_case.attrib == {"name": "E005-required-parameter-missing[0]", "classname": "CheckParameters"}

    def test_junit_xml_add_failure_info(self) -> None:
        root = ET.Element("testsuites")
        test_suite = ET.SubElement(root, "testsuite", name="CheckParameters")
        test_case = ET.SubElement(
            test_suite, "testcase", name="E005-required-parameter-missing[0]", classname="CheckParameters"
        )
        junit_renderer = JUnitXml()
        error_case = junit_renderer.add_failure_info(
            test_case,
            "examples/playbook.yml:5:7: ERROR: [E005] 'name' is a required parameter in module 'sensu.sensu_go.user'. "
            "View docs at https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html for more info.",
            "ERROR",
        )

        assert isinstance(error_case, ET.Element)
        assert error_case.tag == "error"
        assert error_case.attrib == {
            "message": "examples/playbook.yml:5:7: ERROR: [E005] 'name' is a required parameter in module "
            "'sensu.sensu_go.user'. View docs at https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2"
            "/module/user.html for more info.",
            "type": "ERROR",
        }

    def test_junit_xml_add_attribute(self) -> None:
        root = ET.Element("testsuites")
        test_suite = ET.SubElement(root, "testsuite", name="CheckDebug")
        test_case = ET.SubElement(
            test_suite, "testcase", name="H500-debug-module-in-production[1]", classname="CheckDebug"
        )

        junit_renderer = JUnitXml()
        junit_renderer.add_attribute(test_suite, "tests", "1")
        junit_renderer.add_attribute(test_suite, "errors", "1")

        assert test_suite.attrib == {"name": "CheckDebug", "tests": "1", "errors": "1"}

        junit_renderer.add_attribute(test_case, "id", "H500")
        junit_renderer.add_attribute(test_case, "file", "playbook.yml")

        assert test_case.attrib == {
            "name": "H500-debug-module-in-production[1]",
            "classname": "CheckDebug",
            "id": "H500",
            "file": "playbook.yml",
        }

    def test_junit_xml_render(self) -> None:
        check_results = [
            CheckResult(
                correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                original_item={
                    "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                    "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 5,
                        "column": 7,
                        "start_mark_index": 62,
                        "end_mark_index": 190,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="E005",
                    event_value="required-parameter-missing",
                    event_message="Required parameter was not set in this module.",
                    check_class="CheckParameters",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.ERROR,
                message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
                suggestion=None,
                doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
                check_type=CheckType.TASK,
            ),
            CheckResult(
                correlation_id="6603c3b9-d038-4d0d-b5eb-f8eb37c0c093",
                original_item={
                    "task_id": "6603c3b9-d038-4d0d-b5eb-f8eb37c0c093",
                    "task_args": {"name": None, "ansible.builtin.uri": {"url": None, "method": None, "user": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 9,
                        "column": 7,
                        "start_mark_index": 192,
                        "end_mark_index": 330,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=9, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="W003",
                    event_value="deprecated-parameter-with-alternative",
                    event_message="Use of this parameter is deprecated. Use new alternative parameter.",
                    check_class="CheckParameters",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.WARNING,
                message="Use of parameter 'user' is deprecated in module 'ansible.builtin.uri'. "
                "Parameter 'url_username' is a new alternative.",
                suggestion=None,
                doc_url=None,
                check_type=CheckType.TASK,
            ),
            CheckResult(
                correlation_id="95143e70-0d9c-45a1-b8b0-8c03f5e54826",
                original_item={
                    "task_id": "95143e70-0d9c-45a1-b8b0-8c03f5e54826",
                    "task_args": {"name": None, "ansible.builtin.debug": {"msg": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 15,
                        "column": 7,
                        "start_mark_index": 332,
                        "end_mark_index": 404,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=15, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="H500",
                    event_value="debug-module-in-production",
                    event_message="Use of module 'debug' is discouraged in production.",
                    check_class="CheckDebug",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.HINT,
                message="Use of module 'debug' is discouraged in production.",
                suggestion=None,
                doc_url=None,
                check_type=CheckType.TASK,
            ),
        ]

        junit_renderer = JUnitXml()
        output_full = junit_renderer.render(check_results)
        output_no_docs = junit_renderer.render(check_results, disable_docs_url=True)

        with Path("tests/unit/data/reporting/junit.xml").open("r", encoding="utf-8") as f_1:
            output_full_xml = f_1.read()
        assert output_full == output_full_xml

        with Path("tests/unit/data/reporting/junit_no_docs.xml").open("r", encoding="utf-8") as f_2:
            output_no_docs_xml = f_2.read()
        assert output_no_docs == output_no_docs_xml

    def test_sarif_render(self, tmp_path: Path, mock_distribution: Callable[[str, str, Path], Distribution]) -> None:
        with mock.patch(
            "spotter.library.utils.get_distribution",
            return_value=mock_distribution("steampunk-spotter", "0.0.0", tmp_path),
        ):
            check_results = [
                CheckResult(
                    correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                    original_item={
                        "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                        "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                        "spotter_metadata": {
                            "file": "/home/user/Desktop/examples/playbook.yml",
                            "line": 5,
                            "column": 7,
                            "start_mark_index": 62,
                            "end_mark_index": 190,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="E005",
                        event_value="required-parameter-missing",
                        event_message="Required parameter was not set in this module.",
                        check_class="CheckParameters",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.ERROR,
                    message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
                    suggestion=None,
                    doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
                    check_type=CheckType.TASK,
                ),
                CheckResult(
                    correlation_id="6603c3b9-d038-4d0d-b5eb-f8eb37c0c093",
                    original_item={
                        "task_id": "6603c3b9-d038-4d0d-b5eb-f8eb37c0c093",
                        "task_args": {"name": None, "ansible.builtin.uri": {"url": None, "method": None, "user": None}},
                        "spotter_metadata": {
                            "file": "/home/user/Desktop/examples/playbook.yml",
                            "line": 9,
                            "column": 7,
                            "start_mark_index": 192,
                            "end_mark_index": 330,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=9, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="W003",
                        event_value="deprecated-parameter-with-alternative",
                        event_message="Use of this parameter is deprecated. Use new alternative parameter.",
                        check_class="CheckParameters",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.WARNING,
                    message="Use of parameter 'user' is deprecated in module 'ansible.builtin.uri'. "
                    "Parameter 'url_username' is a new alternative.",
                    suggestion=None,
                    doc_url=None,
                    check_type=CheckType.TASK,
                ),
                CheckResult(
                    correlation_id="95143e70-0d9c-45a1-b8b0-8c03f5e54826",
                    original_item={
                        "task_id": "95143e70-0d9c-45a1-b8b0-8c03f5e54826",
                        "task_args": {"name": None, "ansible.builtin.debug": {"msg": None}},
                        "spotter_metadata": {
                            "file": "/home/user/Desktop/examples/playbook.yml",
                            "line": 15,
                            "column": 7,
                            "start_mark_index": 332,
                            "end_mark_index": 404,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=15, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="H500",
                        event_value="debug-module-in-production",
                        event_message="Use of module 'debug' is discouraged in production.",
                        check_class="CheckDebug",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.HINT,
                    message="Use of module 'debug' is discouraged in production.",
                    suggestion=None,
                    doc_url=None,
                    check_type=CheckType.TASK,
                ),
            ]

            sarif_redner = SarifReport()
            output_render = json.loads(sarif_redner.render(check_results))

            with Path("tests/unit/data/reporting/sarif.sarif").open("r", encoding="utf-8") as f_1:
                output_file = json.loads(f_1.read())
            assert output_render == output_file

    def test_sarif_render_no_check_result(
        self, tmp_path: Path, mock_distribution: Callable[[str, str, Path], Distribution]
    ) -> None:
        with mock.patch(
            "spotter.library.utils.get_distribution",
            return_value=mock_distribution("steampunk-spotter", "0.0.0", tmp_path),
        ):
            check_results: list[CheckResult] = []
            sarif_redner = SarifReport()
            output_render = json.loads(sarif_redner.render(check_results))
            with Path("tests/unit/data/reporting/sarif_no_check_result.sarif").open("r", encoding="utf-8") as f_1:
                output_file = json.loads(f_1.read())
            assert output_render == output_file

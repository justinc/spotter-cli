import json
from copy import deepcopy
from io import StringIO
from pathlib import Path
from pathlib import PosixPath
from random import shuffle

import pytest
import ruamel.yaml as ruamel

from spotter.library.environment import Environment
from spotter.library.parsing.parsing import ParsingResult
from spotter.library.rewriting.models import CheckType, RewriteSuggestion
from spotter.library.scanning.check_catalog_info import CheckCatalogInfo
from spotter.library.scanning.check_result import CheckResult
from spotter.library.scanning.display_level import DisplayLevel
from spotter.library.scanning.item_metadata import ItemMetadata
from spotter.library.scanning.origin import Origin
from spotter.library.scanning.output_format import OutputFormat
from spotter.library.scanning.payload import Payload
from spotter.library.scanning.profile import Profile
from spotter.library.scanning.progress import Progress
from spotter.library.scanning.progress_status import ProgressStatus
from spotter.library.scanning.result import Result
from spotter.library.scanning.start_async import StartAsync
from spotter.library.scanning.summary import Summary
from tests.unit.spotter.json_helper import load_json


# pylint: disable=too-many-public-methods,too-many-lines
class TestScanning:
    TEST_CHECK_RESULT = CheckResult(
        correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
        original_item={
            "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
            "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
            "spotter_metadata": {
                "file": "/home/user/Desktop/examples/playbook.yml",
                "line": 5,
                "column": 7,
                "start_mark_index": 62,
                "end_mark_index": 190,
            },
            "spotter_obfuscated": [],
            "spotter_noqa": [],
        },
        metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
        catalog_info=CheckCatalogInfo(
            event_code="E005",
            event_value="required-parameter-missing",
            event_message="Required parameter was not set in this module.",
            check_class="CheckParameters",
            event_subcode=None,
            event_submessage=None,
        ),
        level=DisplayLevel.ERROR,
        message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
        suggestion=None,
        doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
        check_type=CheckType.TASK,
    )

    TEST_SCAN_RESULT = Result(
        uuid="VzhZjnCQT5SaGfd6EQG_Gg",
        user="fgoIxzOKSHWvZZpW6LxDew",
        user_info={"first_name": "John", "last_name": "Doe", "email": "john.doe@example.com", "username": "john_doe"},
        project="tpMQ37z4TnC5SCfS9nctcQ",
        organization="mqeMgx44ne2cPCK9GiVx",
        environment={"id": "2EZm_C9dQWiE-sl7anzXPA"},
        scan_date="2023-07-18T07:05:03.964868Z",
        subscription="kwjW9FgJRy6qVWvGOnJo3g",
        web_urls={
            "organization_url": "https://spotter.xlab.si/organization/org_url/",
            "project_url": "https://spotter.xlab.si/organization/organization_url/projects/proj_url/",
            "scan_url": "https://spotter.xlab.si/organization/org_url/projects/proj_url/scans/scan_url/",
        },
        is_paid=True,
        summary=Summary(scan_time=0.2980048656463623, num_errors=1, num_warnings=1, num_hints=2, status="error"),
        scan_progress=Progress(progress_status=ProgressStatus.SUCCESS, current=10, total=10),
        check_results=[
            CheckResult(
                correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                original_item={
                    "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                    "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 5,
                        "column": 7,
                        "start_mark_index": 62,
                        "end_mark_index": 190,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="E005",
                    event_value="required-parameter-missing",
                    event_message="Required parameter was not set in this module.",
                    check_class="CheckParameters",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.ERROR,
                message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
                suggestion=None,
                doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
                check_type=CheckType.TASK,
            ),
            CheckResult(
                correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                original_item={
                    "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                    "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 5,
                        "column": 7,
                        "start_mark_index": 62,
                        "end_mark_index": 190,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="H1900",
                    event_value="requirements-collection-missing",
                    event_message="Required collection is missing from requirements.yml.",
                    check_class="CheckCollectionRequirements",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.HINT,
                message="Required collection 'sensu.sensu_go' is missing from requirements.yml or "
                "requirements.yml is missing.",
                suggestion=RewriteSuggestion(
                    check_type=CheckType.TASK,
                    item_args={"name": None, "sensu.sensu_go.user": {"password": None}},
                    file=PosixPath("/home/user/Desktop/examples/playbook.yml"),
                    file_parent=PosixPath("/home/user/Desktop/examples"),
                    line=5,
                    column=7,
                    start_mark=62,
                    end_mark=190,
                    suggestion_spec={
                        "data": {"version": "1.13.2", "collection_name": "sensu.sensu_go"},
                        "action": "FIX_REQUIREMENTS",
                    },
                    display_level=DisplayLevel.HINT,
                ),
                doc_url=None,
                check_type=CheckType.TASK,
            ),
            CheckResult(
                correlation_id="6603c3b9-d038-4d0d-b5eb-f8eb37c0c093",
                original_item={
                    "task_id": "6603c3b9-d038-4d0d-b5eb-f8eb37c0c093",
                    "task_args": {"name": None, "ansible.builtin.uri": {"url": None, "method": None, "user": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 9,
                        "column": 7,
                        "start_mark_index": 192,
                        "end_mark_index": 330,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=9, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="W003",
                    event_value="deprecated-parameter-with-alternative",
                    event_message="Use of this parameter is deprecated. Use new alternative parameter.",
                    check_class="CheckParameters",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.WARNING,
                message="Use of parameter 'user' is deprecated in module 'ansible.builtin.uri'. "
                "Parameter 'url_username' is a new alternative.",
                suggestion=None,
                doc_url=None,
                check_type=CheckType.TASK,
            ),
            CheckResult(
                correlation_id="95143e70-0d9c-45a1-b8b0-8c03f5e54826",
                original_item={
                    "task_id": "95143e70-0d9c-45a1-b8b0-8c03f5e54826",
                    "task_args": {"name": None, "ansible.builtin.debug": {"msg": None}},
                    "spotter_metadata": {
                        "file": "/home/user/Desktop/examples/playbook.yml",
                        "line": 15,
                        "column": 7,
                        "start_mark_index": 332,
                        "end_mark_index": 404,
                    },
                    "spotter_obfuscated": [],
                    "spotter_noqa": [],
                },
                metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=15, column=7),
                catalog_info=CheckCatalogInfo(
                    event_code="H500",
                    event_value="debug-module-in-production",
                    event_message="Use of module 'debug' is discouraged in production.",
                    check_class="CheckDebug",
                    event_subcode=None,
                    event_submessage=None,
                ),
                level=DisplayLevel.HINT,
                message="Use of module 'debug' is discouraged in production.",
                suggestion=None,
                doc_url=None,
                check_type=CheckType.TASK,
            ),
        ],
        fixed_check_results=[],
    )

    def test_create_check_catalog_info_from_api_response_element(self) -> None:
        api_response_element_json = load_json("tests/unit/data/scanning/api_response_element.json")
        check_catalog_info = CheckCatalogInfo.from_api_response_element(api_response_element_json)

        assert check_catalog_info.event_code == "E005"
        assert check_catalog_info.event_value == "required-parameter-missing"
        assert check_catalog_info.event_message == "Required parameter was not set in this module."
        assert check_catalog_info.check_class == "CheckParameters"
        assert check_catalog_info.event_subcode is None
        assert check_catalog_info.event_submessage is None

    def test_check_result_construct_output(self) -> None:
        output_full = self.TEST_CHECK_RESULT.construct_output()
        output_no_colors = self.TEST_CHECK_RESULT.construct_output(disable_colors=True)
        output_no_docs = self.TEST_CHECK_RESULT.construct_output(disable_docs_url=True)
        output_no_colors_no_docs = self.TEST_CHECK_RESULT.construct_output(disable_colors=True, disable_docs_url=True)

        with Path("tests/unit/data/scanning/check_output_full.txt").open("r", encoding="unicode_escape") as f_1:
            output_full_txt = f_1.read()
        assert output_full == output_full_txt

        with Path("tests/unit/data/scanning/check_output_no_colors.txt").open("r", encoding="utf-8") as f_2:
            output_no_colors_txt = f_2.read()
        assert output_no_colors == output_no_colors_txt

        with Path("tests/unit/data/scanning/check_output_no_docs.txt").open("r", encoding="unicode_escape") as f_3:
            output_no_docs_txt = f_3.read()
        assert output_no_docs == output_no_docs_txt

        with Path("tests/unit/data/scanning/check_output_no_colors_no_docs.txt").open("r", encoding="utf-8") as f_4:
            output_no_colors_no_docs_txt = f_4.read()
        assert output_no_colors_no_docs == output_no_colors_no_docs_txt

    def test_check_result_construct_output_with_subcode(self) -> None:
        check_result = CheckResult(
            correlation_id="f6358590-5518-413e-913a-2dc20cfa111a",
            original_item={
                "task_id": "f6358590-5518-413e-913a-2dc20cfa111a",
                "task_args": {
                    "name": 'Create a new database with name "test_db"',
                    "community.postgresql.postgresql_db": {"name": "test_db"},
                },
                "spotter_metadata": {
                    "file": "/home/user/Desktop/examples/playbook.yml",
                    "line": 1,
                    "column": 3,
                    "start_mark_index": 2,
                    "end_mark_index": 106,
                },
                "spotter_obfuscated": [],
                "spotter_noqa": [],
            },
            metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=1, column=3),
            catalog_info=CheckCatalogInfo(
                event_code="W2600",
                event_value="issue-found-in-implementation-of-module",
                event_message="Issue found in the Python implementation of module.",
                check_class="CheckBandit",
                event_subcode="B602",
                event_submessage="subprocess_popen_with_shell_equals_true",
            ),
            level=DisplayLevel.WARNING,
            message="Issue found in the Python implementation of module 'community.postgresql.postgresql_db': "
            "subprocess call with shell=True identified, security issue.",
            suggestion=None,
            doc_url=None,
            check_type=CheckType.TASK,
        )

        output_full = check_result.construct_output()
        output_no_colors = check_result.construct_output(disable_colors=True)

        with Path("tests/unit/data/scanning/check_output_subcode_full.txt").open("r", encoding="unicode_escape") as f_1:
            output_full_txt = f_1.read()
        assert output_full == output_full_txt

        with Path("tests/unit/data/scanning/check_output_subcode_no_colors.txt").open("r", encoding="utf-8") as f_2:
            output_no_colors_txt = f_2.read()
        assert output_no_colors == output_no_colors_txt

    def test_create_display_level_from_string(self) -> None:
        display_level = DisplayLevel.from_string("warning")

        assert display_level.name == "WARNING"
        assert display_level.value == 2

    def test_create_display_level_from_string_nonexistent_level(self) -> None:
        with pytest.raises(SystemExit) as exception_info:
            DisplayLevel.from_string("fatal")
        assert exception_info.value.code == 2

    def test_create_item_metadata_from_item_meta(self) -> None:
        item_metadata = ItemMetadata.from_item_meta(
            {
                "file": "/home/user/Desktop/examples/playbook.yml",
                "line": 5,
                "column": 7,
                "start_mark_index": 62,
                "end_mark_index": 190,
            }
        )

        assert item_metadata.file_name == "/home/user/Desktop/examples/playbook.yml"
        assert item_metadata.line == 5
        assert item_metadata.column == 7

    def test_create_origin_from_string(self) -> None:
        origin = Origin.from_string("cli")

        assert origin.name == "CLI"
        assert origin.value == 1

    def test_create_origin_from_string_nonexistent_origin(self) -> None:
        with pytest.raises(SystemExit) as exception_info:
            Origin.from_string("os")
        assert exception_info.value.code == 2

    def test_create_output_format_from_string(self) -> None:
        output_format = OutputFormat.from_string("yaml")

        assert output_format.name == "YAML"
        assert output_format.value == 3

    def test_create_output_format_from_string_nonexistent_format(self) -> None:
        with pytest.raises(SystemExit) as exception_info:
            OutputFormat.from_string("toml")
        assert exception_info.value.code == 2

    def test_create_payload_from_json_file(self) -> None:
        payload_json = load_json("tests/unit/data/scanning/payload_export.json")
        payload = Payload.from_json_file(Path("tests/unit/data/scanning/payload_export.json"))

        assert payload.environment == Environment(**payload_json.get("environment", {}))
        assert payload.tasks == payload_json.get("tasks", [])
        assert payload.playbooks == payload_json.get("playbooks", [])

    def test_create_payload_from_args(self) -> None:
        payload_json = load_json("tests/unit/data/scanning/payload_export.json")
        payload_json_environment = Environment(**payload_json.get("environment", {}))
        payload_json_tasks = payload_json.get("tasks", [])
        payload_json_playbooks = payload_json.get("playbooks", [])

        payload = Payload.from_args(
            ParsingResult(tasks=payload_json_tasks, playbooks=payload_json_playbooks, errors=[]),
            payload_json_environment,
            True,
            None,
        )

        assert payload.environment == payload_json_environment
        assert payload.tasks == payload_json_tasks
        assert payload.playbooks == payload_json_playbooks

    def test_export_payload_to_json_file(self, tmp_path: Path) -> None:
        payload_json = load_json("tests/unit/data/scanning/payload_export_without_nan.json")

        payload = Payload(**payload_json)
        export_path = tmp_path / "payload.json"
        payload.to_json_file(tmp_path / "payload.json")

        assert load_json(str(export_path)) == payload_json

    def test_create_profile_from_string(self) -> None:
        profile = Profile.from_string("full")

        assert profile.name == "FULL"
        assert profile.value == 1

    def test_create_profile_from_string_nonexistent_format(self) -> None:
        with pytest.raises(SystemExit) as exception_info:
            Profile.from_string("advanced")
        assert exception_info.value.code == 2

    def test_create_progress_from_api_response_element(self) -> None:
        progress = Progress.from_api_response_element({"progress_status": "pending", "current": 7, "total": 10})

        assert progress.progress_status.name == "PENDING"
        assert progress.progress_status.value == 0
        assert progress.current == 7
        assert progress.total == 10

    def test_create_progress_status_from_status(self) -> None:
        progress_status = ProgressStatus.from_string("started")

        assert progress_status.name == "STARTED"
        assert progress_status.value == 1

    def test_create_progress_status_from_string_nonexistent_status(self) -> None:
        with pytest.raises(SystemExit) as exception_info:
            ProgressStatus.from_string("waiting")
        assert exception_info.value.code == 2

    def test_create_result_from_api_response(self) -> None:
        api_response_json = load_json("tests/unit/data/scanning/api_response.json")
        input_tasks_json = load_json("tests/unit/data/scanning/input_tasks.json")
        input_playbooks_json = load_json("tests/unit/data/scanning/input_playbooks.json")
        result = Result.from_api_response(api_response_json, input_tasks_json, input_playbooks_json, 0.2980048656463623)

        assert result == self.TEST_SCAN_RESULT

    def test_parse_known_check_result(self) -> None:
        api_response_element_json = load_json("tests/unit/data/scanning/api_response_element.json")
        input_task_items_json = load_json("tests/unit/data/scanning/input_task_items.json")
        # pylint: disable=protected-access
        check_result = Result._parse_known_check_result(api_response_element_json, input_task_items_json)

        assert check_result == self.TEST_CHECK_RESULT

    def test_parse_unknown_check_result(self) -> None:
        api_response_element_json = load_json("tests/unit/data/scanning/api_response_element.json")
        # pylint: disable=protected-access
        check_result = Result._parse_unknown_check_result(api_response_element_json)

        assert check_result == CheckResult(
            correlation_id="",
            original_item={},
            metadata=None,
            catalog_info=CheckCatalogInfo(
                event_code="E005",
                event_value="required-parameter-missing",
                event_message="Required parameter was not set in this module.",
                check_class="CheckParameters",
                event_subcode=None,
                event_submessage=None,
            ),
            level=DisplayLevel.ERROR,
            message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
            suggestion=None,
            doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
            check_type=CheckType.TASK,
        )

    def test_parse_check_results(self) -> None:
        api_response_json = load_json("tests/unit/data/scanning/api_response.json")
        input_tasks_json = load_json("tests/unit/data/scanning/input_tasks.json")
        input_playbooks_json = load_json("tests/unit/data/scanning/input_playbooks.json")
        result = Result(
            uuid="VzhZjnCQT5SaGfd6EQG_Gg",
            user="fgoIxzOKSHWvZZpW6LxDew",
            user_info={
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
                "username": "john_doe",
            },
            project="tpMQ37z4TnC5SCfS9nctcQ",
            organization="mqeMgx44ne2cPCK9GiVx",
            environment={"id": "2EZm_C9dQWiE-sl7anzXPA"},
            scan_date="2023-07-18T07:05:03.964868Z",
            subscription="kwjW9FgJRy6qVWvGOnJo3g",
            is_paid=True,
            web_urls={
                "organization_url": "https://spotter.xlab.si/organization/org_url/",
                "project_url": "https://spotter.xlab.si/organization/organization_url/projects/proj_url/",
                "scan_url": "https://spotter.xlab.si/organization/org_url/projects/proj_url/scans/scan_url/",
            },
            summary=Summary(scan_time=0.2980048656463623, num_errors=0, num_warnings=0, num_hints=0, status="error"),
            scan_progress=Progress(progress_status=ProgressStatus.SUCCESS, current=10, total=10),
            check_results=[],
            fixed_check_results=[],
        )
        result.parse_check_results(api_response_json, input_tasks_json, input_playbooks_json)

        assert result == self.TEST_SCAN_RESULT

    def test_filter_check_results(self) -> None:
        result = deepcopy(self.TEST_SCAN_RESULT)
        result.filter_check_results(DisplayLevel.ERROR)

        assert result.check_results == [self.TEST_CHECK_RESULT]

    def test_get_sort_key(self) -> None:
        # pylint: disable=protected-access
        sort_keys_tuple = Result._get_sort_key(self.TEST_CHECK_RESULT)
        assert sort_keys_tuple == ("/home/user/Desktop/examples/playbook.yml", 5, 7, -3)

    def test_get_sort_key_no_metadata(self) -> None:
        check_result = CheckResult(
            correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
            original_item={
                "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                "spotter_metadata": {
                    "file": "/home/user/Desktop/examples/playbook.yml",
                    "line": 5,
                    "column": 7,
                    "start_mark_index": 62,
                    "end_mark_index": 190,
                },
                "spotter_obfuscated": [],
                "spotter_noqa": [],
            },
            metadata=None,
            catalog_info=CheckCatalogInfo(
                event_code="E005",
                event_value="required-parameter-missing",
                event_message="Required parameter was not set in this module.",
                check_class="CheckParameters",
                event_subcode=None,
                event_submessage=None,
            ),
            level=DisplayLevel.ERROR,
            message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
            suggestion=None,
            doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
            check_type=CheckType.TASK,
        )
        # pylint: disable=protected-access
        sort_keys_tuple = Result._get_sort_key(check_result)

        assert sort_keys_tuple == ("", 0, 0, 0)

    def test_sort_check_results(self) -> None:
        result_to_sort = deepcopy(self.TEST_SCAN_RESULT)
        shuffle(result_to_sort.check_results)
        result_to_sort.sort_check_results()

        assert result_to_sort.check_results == self.TEST_SCAN_RESULT.check_results

    def test_result_format_text_full(self) -> None:
        # pylint: disable=protected-access
        output_full = self.TEST_SCAN_RESULT._format_text()

        with Path("tests/unit/data/scanning/scan_output_full.txt").open("r", encoding="unicode_escape") as f:
            output_full_txt = f.read()
        assert output_full == output_full_txt

    def test_result_format_text_no_check_results(self) -> None:
        # pylint: disable=protected-access
        scan_result_with_no_check_results = Result(
            uuid="VzhZjnCQT5SaGfd6EQG_Gg",
            user="fgoIxzOKSHWvZZpW6LxDew",
            user_info={
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
                "username": "john_doe",
            },
            project="tpMQ37z4TnC5SCfS9nctcQ",
            organization="mqeMgx44ne2cPCK9GiVx",
            environment={"id": "2EZm_C9dQWiE-sl7anzXPA"},
            scan_date="2023-07-18T07:05:03.964868Z",
            subscription="kwjW9FgJRy6qVWvGOnJo3g",
            is_paid=True,
            web_urls={
                "organization_url": "https://spotter.xlab.si/organization/org_url/",
                "project_url": "https://spotter.xlab.si/organization/organization_url/projects/proj_url/",
                "scan_url": "https://spotter.xlab.si/organization/org_url/projects/proj_url/scans/scan_url/",
            },
            summary=Summary(scan_time=0.2980048656463623, num_errors=1, num_warnings=1, num_hints=2, status="error"),
            scan_progress=Progress(progress_status=ProgressStatus.SUCCESS, current=10, total=10),
            check_results=[],
            fixed_check_results=[],
        )
        output_no_check_results = scan_result_with_no_check_results._format_text()

        with Path("tests/unit/data/scanning/scan_output_no_check_results.txt").open(
            "r", encoding="unicode_escape"
        ) as f:
            output_output_no_check_results_txt = f.read()
        assert output_no_check_results == output_output_no_check_results_txt

    def test_result_format_text_with_fixed_check_results(self) -> None:
        # pylint: disable=protected-access
        scan_result_with_fixed_check_results = Result(
            uuid="VzhZjnCQT5SaGfd6EQG_Gg",
            user="fgoIxzOKSHWvZZpW6LxDew",
            user_info={
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
                "username": "john_doe",
            },
            project="tpMQ37z4TnC5SCfS9nctcQ",
            organization="mqeMgx44ne2cPCK9GiVx",
            environment={"id": "2EZm_C9dQWiE-sl7anzXPA"},
            scan_date="2023-07-18T07:05:03.964868Z",
            subscription="kwjW9FgJRy6qVWvGOnJo3g",
            is_paid=True,
            web_urls={
                "organization_url": "https://spotter.xlab.si/organization/org_url/",
                "project_url": "https://spotter.xlab.si/organization/organization_url/projects/proj_url/",
                "scan_url": "https://spotter.xlab.si/organization/org_url/projects/proj_url/scans/scan_url/",
            },
            summary=Summary(scan_time=0.2980048656463623, num_errors=1, num_warnings=1, num_hints=2, status="error"),
            scan_progress=Progress(progress_status=ProgressStatus.SUCCESS, current=10, total=10),
            check_results=[
                CheckResult(
                    correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                    original_item={
                        "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                        "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                        "spotter_metadata": {
                            "file": "/home/user/Desktop/examples/playbook.yml",
                            "line": 5,
                            "column": 7,
                            "start_mark_index": 62,
                            "end_mark_index": 190,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="E005",
                        event_value="required-parameter-missing",
                        event_message="Required parameter was not set in this module.",
                        check_class="CheckParameters",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.ERROR,
                    message="'name' is a required parameter in module 'sensu.sensu_go.user'.",
                    suggestion=None,
                    doc_url="https://docs.steampunk.si/plugins/sensu/sensu_go/1.13.2/module/user.html",
                    check_type=CheckType.TASK,
                ),
            ],
            fixed_check_results=[
                CheckResult(
                    correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                    original_item={
                        "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                        "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                        "spotter_metadata": {
                            "file": "/home/user/Desktop/examples/playbook.yml",
                            "line": 5,
                            "column": 7,
                            "start_mark_index": 62,
                            "end_mark_index": 190,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="H1900",
                        event_value="requirements-collection-missing",
                        event_message="Required collection is missing from requirements.yml.",
                        check_class="CheckCollectionRequirements",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.HINT,
                    message="Required collection 'sensu.sensu_go' is missing from requirements.yml or "
                    "requirements.yml is missing.",
                    suggestion=RewriteSuggestion(
                        check_type=CheckType.TASK,
                        item_args={"name": None, "sensu.sensu_go.user": {"password": None}},
                        file=PosixPath("/home/user/Desktop/examples/playbook.yml"),
                        file_parent=PosixPath("/home/user/Desktop/examples"),
                        line=0,
                        column=0,
                        start_mark=62,
                        end_mark=190,
                        suggestion_spec={
                            "data": {"version": "1.13.2", "collection_name": "sensu.sensu_go"},
                            "action": "FIX_REQUIREMENTS",
                        },
                        display_level=DisplayLevel.HINT,
                    ),
                    doc_url=None,
                    check_type=CheckType.TASK,
                ),
            ],
        )
        output_full = scan_result_with_fixed_check_results._format_text()

        with Path("tests/unit/data/scanning/scan_output_with_fixed_check_results.txt").open(
            "r", encoding="unicode_escape"
        ) as f:
            output_full_txt = f.read()
        assert output_full == output_full_txt

    def test_result_format_text_no_check_results_with_fixed_check_results(self) -> None:
        # pylint: disable=protected-access
        scan_result_with_fixed_check_results = Result(
            uuid="VzhZjnCQT5SaGfd6EQG_Gg",
            user="fgoIxzOKSHWvZZpW6LxDew",
            user_info={
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
                "username": "john_doe",
            },
            project="tpMQ37z4TnC5SCfS9nctcQ",
            organization="mqeMgx44ne2cPCK9GiVx",
            environment={"id": "2EZm_C9dQWiE-sl7anzXPA"},
            scan_date="2023-07-18T07:05:03.964868Z",
            subscription="kwjW9FgJRy6qVWvGOnJo3g",
            is_paid=True,
            web_urls={
                "organization_url": "https://spotter.xlab.si/organization/org_url/",
                "project_url": "https://spotter.xlab.si/organization/organization_url/projects/proj_url/",
                "scan_url": "https://spotter.xlab.si/organization/org_url/projects/proj_url/scans/scan_url/",
            },
            summary=Summary(scan_time=0.2980048656463623, num_errors=1, num_warnings=1, num_hints=2, status="error"),
            scan_progress=Progress(progress_status=ProgressStatus.SUCCESS, current=10, total=10),
            check_results=[],
            fixed_check_results=[
                CheckResult(
                    correlation_id="4a74782b-5224-4c7d-85ff-372827e3f640",
                    original_item={
                        "task_id": "4a74782b-5224-4c7d-85ff-372827e3f640",
                        "task_args": {"name": None, "sensu.sensu_go.user": {"password": None}},
                        "spotter_metadata": {
                            "file": "/home/user/Desktop/examples/playbook.yml",
                            "line": 5,
                            "column": 7,
                            "start_mark_index": 62,
                            "end_mark_index": 190,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name="/home/user/Desktop/examples/playbook.yml", line=5, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="H1900",
                        event_value="requirements-collection-missing",
                        event_message="Required collection is missing from requirements.yml.",
                        check_class="CheckCollectionRequirements",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.HINT,
                    message="Required collection 'sensu.sensu_go' is missing from requirements.yml or "
                    "requirements.yml is missing.",
                    suggestion=RewriteSuggestion(
                        check_type=CheckType.TASK,
                        item_args={"name": None, "sensu.sensu_go.user": {"password": None}},
                        file=PosixPath("/home/user/Desktop/examples/playbook.yml"),
                        file_parent=PosixPath("/home/user/Desktop/examples"),
                        line=0,
                        column=0,
                        start_mark=62,
                        end_mark=190,
                        suggestion_spec={
                            "data": {"version": "1.13.2", "collection_name": "sensu.sensu_go"},
                            "action": "FIX_REQUIREMENTS",
                        },
                        display_level=DisplayLevel.HINT,
                    ),
                    doc_url=None,
                    check_type=CheckType.TASK,
                ),
            ],
        )
        output_full = scan_result_with_fixed_check_results._format_text()

        with Path("tests/unit/data/scanning/scan_output_no_check_results_with_fixed_check_results.txt").open(
            "r", encoding="unicode_escape"
        ) as f:
            output_full_txt = f.read()
        assert output_full == output_full_txt

    def test_result_format_text_no_colors(self) -> None:
        # pylint: disable=protected-access
        output_no_colors = self.TEST_SCAN_RESULT._format_text(disable_colors=True)

        with Path("tests/unit/data/scanning/scan_output_no_colors.txt").open("r", encoding="utf-8") as f:
            output_no_colors_txt = f.read()
        assert output_no_colors == output_no_colors_txt

    def test_result_format_text_no_docs(self) -> None:
        # pylint: disable=protected-access
        output_no_docs = self.TEST_SCAN_RESULT._format_text(disable_docs_url=True)

        with Path("tests/unit/data/scanning/scan_output_no_docs.txt").open("r", encoding="unicode_escape") as f:
            output_no_docs_txt = f.read()
        assert output_no_docs == output_no_docs_txt

    def test_result_format_text_no_scan_url(self) -> None:
        # pylint: disable=protected-access
        output_no_scan_url = self.TEST_SCAN_RESULT._format_text(disable_scan_url=True)

        with Path("tests/unit/data/scanning/scan_output_no_scan_url.txt").open("r", encoding="unicode_escape") as f:
            output_no_scan_url_txt = f.read()
        assert output_no_scan_url == output_no_scan_url_txt

    def test_result_format_text_no_colors_no_docs(self) -> None:
        # pylint: disable=protected-access
        output_no_colors_no_docs = self.TEST_SCAN_RESULT._format_text(disable_colors=True, disable_docs_url=True)

        with Path("tests/unit/data/scanning/scan_output_no_colors_no_docs.txt").open("r", encoding="utf-8") as f:
            output_no_colors_no_docs_txt = f.read()
        assert output_no_colors_no_docs == output_no_colors_no_docs_txt

    def test_result_format_text_no_colors_no_docs_no_scan_url(self) -> None:
        # pylint: disable=protected-access
        output_no_colors_no_docs_no_scan_url = self.TEST_SCAN_RESULT._format_text(
            disable_colors=True, disable_docs_url=True, disable_scan_url=True
        )

        with Path("tests/unit/data/scanning/scan_output_no_colors_no_docs_no_scan_url.txt").open(
            "r", encoding="utf-8"
        ) as f:
            output_no_colors_no_docs_no_scan_url_txt = f.read()
        assert output_no_colors_no_docs_no_scan_url == output_no_colors_no_docs_no_scan_url_txt

    def test_result_format_dict_full(self) -> None:
        # pylint: disable=protected-access
        output_full = self.TEST_SCAN_RESULT._format_dict()

        assert output_full == load_json("tests/unit/data/scanning/scan_output_dict.json")

    def test_result_format_dict_no_docs(self) -> None:
        # pylint: disable=protected-access
        output_no_docs = self.TEST_SCAN_RESULT._format_dict(disable_docs_url=True)

        assert output_no_docs == load_json("tests/unit/data/scanning/scan_output_dict_no_docs.json")

    def test_result_format_dict_no_web_urls(self) -> None:
        # pylint: disable=protected-access
        output_no_web_urls = self.TEST_SCAN_RESULT._format_dict(disable_scan_url=True)

        assert output_no_web_urls == load_json("tests/unit/data/scanning/scan_output_dict_no_web_urls.json")

    def test_result_format_json_full(self) -> None:
        # pylint: disable=protected-access
        output_full = self.TEST_SCAN_RESULT._format_json()

        output_full_json = load_json("tests/unit/data/scanning/scan_output_dict.json")

        assert output_full == json.dumps(output_full_json, indent=2)

    def test_result_format_json_no_docs(self) -> None:
        # pylint: disable=protected-access
        output_no_docs = self.TEST_SCAN_RESULT._format_json(disable_docs_url=True)

        output_no_docs_json = load_json("tests/unit/data/scanning/scan_output_dict_no_docs.json")

        assert output_no_docs == json.dumps(output_no_docs_json, indent=2)

    def test_result_format_json_no_web_urls(self) -> None:
        # pylint: disable=protected-access
        output_no_web_urls = self.TEST_SCAN_RESULT._format_dict(disable_scan_url=True)

        assert output_no_web_urls == load_json("tests/unit/data/scanning/scan_output_dict_no_web_urls.json")

    def test_result_format_yaml_full(self) -> None:
        # pylint: disable=protected-access
        output_full = self.TEST_SCAN_RESULT._format_yaml()
        output_full_json = load_json("tests/unit/data/scanning/scan_output_dict.json")

        yaml = ruamel.YAML(typ="rt")
        yaml.indent(mapping=2, sequence=4, offset=2)
        output_full_yaml = StringIO()
        yaml.dump(output_full_json, output_full_yaml)

        assert output_full == output_full_yaml.getvalue()

    def test_result_format_yaml_no_docs(self) -> None:
        # pylint: disable=protected-access
        output_no_docs = self.TEST_SCAN_RESULT._format_yaml(disable_docs_url=True)
        output_no_docs_json = load_json("tests/unit/data/scanning/scan_output_dict_no_docs.json")

        yaml = ruamel.YAML(typ="rt")
        yaml.indent(mapping=2, sequence=4, offset=2)
        output_no_docs_yaml = StringIO()
        yaml.dump(output_no_docs_json, output_no_docs_yaml)

        assert output_no_docs == output_no_docs_yaml.getvalue()

    def test_result_format_yaml_no_web_urls(self) -> None:
        # pylint: disable=protected-access
        output_no_web_urls = self.TEST_SCAN_RESULT._format_yaml(disable_scan_url=True)
        output_no_web_urls_json = load_json("tests/unit/data/scanning/scan_output_dict_no_web_urls.json")

        yaml = ruamel.YAML(typ="rt")
        yaml.indent(mapping=2, sequence=4, offset=2)
        output_no_docs_yaml = StringIO()
        yaml.dump(output_no_web_urls_json, output_no_docs_yaml)

        assert output_no_web_urls == output_no_docs_yaml.getvalue()

    def test_result_format_junit_xml_full(self) -> None:
        # pylint: disable=protected-access
        output_full = self.TEST_SCAN_RESULT.format_junit_xml()

        with Path("tests/unit/data/scanning/scan_output_junit.xml").open("r", encoding="utf-8") as f:
            output_full_xml = f.read()
        assert output_full == output_full_xml

    def test_result_format_junit_xml_no_docs(self) -> None:
        # pylint: disable=protected-access
        output_no_docs = self.TEST_SCAN_RESULT.format_junit_xml(disable_docs_url=True)

        with Path("tests/unit/data/scanning/scan_output_junit_no_docs.xml").open("r", encoding="utf-8") as f:
            output_no_docs_xml = f.read()
        assert output_no_docs == output_no_docs_xml

    def test_apply_check_result_suggestions(self, tmp_path: Path) -> None:
        ansible_playbook_before_rewrite = [
            {
                "name": "OpenSSL example",
                "hosts": "localhost",
                "tasks": [
                    {
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "openssl_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    }
                ],
            }
        ]
        ansible_playbook_after_rewrite = [
            {
                "name": "OpenSSL example",
                "hosts": "localhost",
                "tasks": [
                    {
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "community.crypto.x509_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    }
                ],
            }
        ]

        yaml = ruamel.YAML(typ="rt")
        playbook_path = tmp_path / "playbook.yml"
        yaml.dump(ansible_playbook_before_rewrite, playbook_path)

        scan_result = Result(
            uuid="VsMuxi0MR2ikzZFejRJ2YA",
            user="fgoIxzOKSHWvZZpW6LxDew",
            user_info={
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
                "username": "john_doe",
            },
            project="tpMQ37z4TnC5SCfS9nctcQ",
            organization="mqeMgx44ne2cPCK9GiVx",
            environment={"id": "8L67qsQiStK7ahsi0mhsxQ"},
            scan_date="2023-07-18T13:15:37.933172Z",
            subscription="kwjW9FgJRy6qVWvGOnJo3g",
            is_paid=True,
            summary=Summary(scan_time=0.41387248039245605, num_errors=1, num_warnings=0, num_hints=0, status="error"),
            scan_progress=Progress(progress_status=ProgressStatus.SUCCESS, current=10, total=10),
            check_results=[
                CheckResult(
                    correlation_id="aa321a2d-1aba-422c-ae95-197eaa3dc4d3",
                    original_item={
                        "task_id": "aa321a2d-1aba-422c-ae95-197eaa3dc4d3",
                        "task_args": {
                            "name": None,
                            "openssl_certificate": {"path": None, "privatekey_path": None, "provider": None},
                        },
                        "spotter_metadata": {
                            "file": str(playbook_path),
                            "line": 4,
                            "column": 7,
                            "start_mark_index": 58,
                            "end_mark_index": 319,
                        },
                        "spotter_obfuscated": [],
                        "spotter_noqa": [],
                    },
                    metadata=ItemMetadata(file_name=str(playbook_path), line=4, column=7),
                    catalog_info=CheckCatalogInfo(
                        event_code="E903",
                        event_value="short-name-used-error",
                        event_message="Use a fully-qualified name instead of short name.",
                        check_class="CheckFQCN",
                        event_subcode=None,
                        event_submessage=None,
                    ),
                    level=DisplayLevel.ERROR,
                    message="Use a fully-qualified name, such as 'community.crypto.x509_certificate' "
                    "instead of 'openssl_certificate'.",
                    suggestion=RewriteSuggestion(
                        check_type=CheckType.TASK,
                        item_args={
                            "name": None,
                            "openssl_certificate": {"path": None, "privatekey_path": None, "provider": None},
                        },
                        file=playbook_path,
                        file_parent=playbook_path.parent,
                        line=4,
                        column=7,
                        start_mark=58,
                        end_mark=319,
                        suggestion_spec={
                            "data": {"after": "community.crypto.x509_certificate", "before": "openssl_certificate"},
                            "action": "FIX_FQCN",
                        },
                        display_level=DisplayLevel.ERROR,
                    ),
                    doc_url="https://docs.steampunk.si/plugins/community/crypto/latest/module/x509_certificate.html",
                    check_type=CheckType.TASK,
                )
            ],
            fixed_check_results=[],
        )

        scan_result.apply_check_result_suggestions(DisplayLevel.SUCCESS)
        with playbook_path.open("r", encoding="utf-8") as f:
            playbook_rewritten = StringIO()
            yaml.dump(ruamel.YAML(typ="safe").load(f), playbook_rewritten)

        playbook_after_rewrite = StringIO()
        yaml.dump(ansible_playbook_after_rewrite, playbook_after_rewrite)
        assert playbook_rewritten.getvalue() == playbook_after_rewrite.getvalue()

    def test_create_scan_start_async_from_api_response(self) -> None:
        api_response_element_json = {
            "id": "C58x2-M1Ql-qHMrORK0c1g",
            "project_id": "WOg6BeqoQZqIByHPkNXqVA",
            "organization_id": "gPFJtB4JTtOSvpKgHRWWAw",
            "scan_progress": {"progress_status": "failure", "current": 0, "total": 10},
        }
        start_async = StartAsync.from_api_response(api_response_element_json)

        assert start_async.uuid == "C58x2-M1Ql-qHMrORK0c1g"
        assert start_async.project_id == "WOg6BeqoQZqIByHPkNXqVA"
        assert start_async.organization_id == "gPFJtB4JTtOSvpKgHRWWAw"
        assert start_async.scan_progress == Progress(progress_status=ProgressStatus.FAILURE, current=0, total=10)

    def test_update_summary(self) -> None:
        summary = Summary(scan_time=0.2980048656463623, num_errors=0, num_warnings=0, num_hints=0, status="error")
        summary.update(self.TEST_CHECK_RESULT)

        assert summary == Summary(
            scan_time=0.2980048656463623, num_errors=1, num_warnings=0, num_hints=0, status="error"
        )

import sys
from pathlib import Path
from typing import Callable, Any, List, Optional
from unittest import mock

import pytest
import ruamel.yaml as ruamel
import ruamel.yaml.error as yaml_error

from spotter.library.parsing import parsing, noqa_comments
from spotter.library.rewriting.models import CheckType
from spotter.library.scanning.parser_error import YamlErrorDetails
from tests.unit.spotter.json_helper import load_json, load_yaml


# pylint: disable=too-many-public-methods,too-many-lines
class TestParsing:
    @pytest.fixture()
    def create_ansible_task_file(self) -> Callable[[Path], None]:
        def _create_ansible_task_file(path: Path) -> None:
            task_file_yml_content = [
                {"name": "Print a message", "ansible.builtin.debug": {"msg": "{{ my_message }}"}, "check_mode": True},
                {"name": "Include other play", "ansible.builtin.include": "otherplay.yml", "check_mode": True},
                {"name": "List Sensu users", "sensu.sensu_go.user_info": None, "register": "sensu_users"},
            ]
            ruamel.YAML(typ="rt").dump(task_file_yml_content, path)

        return _create_ansible_task_file

    @pytest.fixture()
    def create_ansible_playbook(self) -> Callable[[Path], None]:
        def _create_ansible_playbook(path: Path) -> None:
            playbook_yml_content = [
                {
                    "name": "Sample playbook",
                    "hosts": "localhost",
                    "collections": ["sensu.sensu_go", "ansible.builtin"],
                    "post_tasks": [{"name": "Post-Task", "ansible.builtin.command": "echo post_task"}],
                    "pre_tasks": [{"name": "Pre-Task", "ansible.builtin.command": "echo pre_task"}],
                    "tasks": [
                        {"name": "Create local user", "sensu.sensu_go.user": {"name": "johnmcclane"}},
                        {"name": "OpenStack info gathering", "openstack.cloud.os_flavor_info": None},
                        {"name": "Read-only command", "ansible.builtin.command": "ls /tmp", "changed_when": False},
                    ],
                }
            ]
            ruamel.YAML(typ="rt").dump(playbook_yml_content, path)

        return _create_ansible_playbook

    @pytest.fixture()
    def create_ansible_role(self, create_ansible_task_file: Callable[[Path], None]) -> Callable[[Path], None]:
        def _create_ansible_role(path: Path) -> None:
            role_tasks_path = path / "tasks"
            role_vars_path = path / "vars"
            role_meta_path = path / "meta"

            path.mkdir(parents=True, exist_ok=True)
            role_tasks_path.mkdir(parents=True, exist_ok=True)
            role_vars_path.mkdir(parents=True, exist_ok=True)
            role_meta_path.mkdir(parents=True, exist_ok=True)

            create_ansible_task_file(role_tasks_path / "main.yml")

            vars_file_path = role_vars_path / "main.yml"
            vars_file_yml_content = [
                {
                    "my_message": "Hello from Ansible",
                }
            ]
            yaml = ruamel.YAML(typ="rt")
            yaml.dump(vars_file_yml_content, vars_file_path)

            meta_file_path = role_vars_path / "main.yml"
            meta_file_yml_content = {
                "dependencies": [],
                "galaxy_info": {
                    "role_name": "example",
                    "author": "Steampunk Spotter",
                    "description": "Example role by Steampunk Spotter.",
                    "company": "XLAB Steampunk",
                    "license": "license (Apache)",
                    "min_ansible_version": 2.14,
                    "platforms": [
                        {"name": "Debian", "versions": ["all"]},
                        {"name": "Ubuntu", "versions": ["focal", "jammy"]},
                    ],
                    "galaxy_tags": ["example", "steampunk", "spotter"],
                },
            }
            yaml.dump(meta_file_yml_content, meta_file_path)

        return _create_ansible_role

    @pytest.fixture()
    def create_ansible_collection(
        self, create_ansible_playbook: Callable[[Path], None], create_ansible_role: Callable[[Path], None]
    ) -> Callable[[Path], None]:
        def _create_ansible_collection(path: Path) -> None:
            roles_path = path / "roles"
            playbooks_path = path / "playbooks"
            integration_tests_path = path / "tests" / "integration" / "targets"

            path.mkdir(parents=True, exist_ok=True)
            roles_path.mkdir(parents=True, exist_ok=True)
            playbooks_path.mkdir(parents=True, exist_ok=True)
            integration_tests_path.mkdir(parents=True, exist_ok=True)

            galaxy_yml_path = path / "galaxy.yml"
            galaxy_yml_content = {
                "namespace": "steampunk_spotter",
                "name": "example",
                "readme": "README.md",
                "authors": "XLAB Steampunk",
                "description": "Steampunk Spotter example for galaxy.yml.",
                "license": "GPL-3.0-or-later",
                "tags": ["example", "steampunk", "spotter"],
                "dependencies": {},
                "build_ignore": [".gitignore", ".env", ".vscode"],
            }
            ruamel.YAML(typ="rt").dump(galaxy_yml_content, galaxy_yml_path)

            create_ansible_role(roles_path / "example")
            create_ansible_playbook(path / "playbook.yml")
            create_ansible_playbook(playbooks_path / "playbook.yml")
            create_ansible_role(integration_tests_path / "example")

        return _create_ansible_collection

    def test_parsing_result_tasks_without_metadata(self) -> None:
        tasks = [
            {
                "task_id": "9429cb0c-4c67-415c-9d55-fdd64ee8e42d",
                "play_id": None,
                "task_args": {"name": None, "sensu.sensu_go.user": {"name": None}},
                "spotter_metadata": {
                    "file": "/tmp/playbook.yaml",
                    "line": 16,
                    "column": 7,
                    "start_mark_index": 122,
                    "end_mark_index": 204,
                },
                "spotter_noqa": [],
            },
            {
                "task_id": "97603529-2b2f-48c8-ac52-341b4f5bfd8c",
                "play_id": None,
                "task_args": {"name": None, "openstack.cloud.os_flavor_info": None},
                "spotter_metadata": {
                    "file": "/tmp/playbook.yaml",
                    "line": 14,
                    "column": 7,
                    "start_mark_index": 206,
                    "end_mark_index": 280,
                },
                "spotter_noqa": [],
            },
        ]
        parsing_result = parsing.ParsingResult(tasks=tasks, playbooks=[], errors=[])

        assert parsing_result.tasks_without_metadata() == [
            {
                "task_id": "9429cb0c-4c67-415c-9d55-fdd64ee8e42d",
                "play_id": None,
                "task_args": {"name": None, "sensu.sensu_go.user": {"name": None}},
                "spotter_noqa": [],
            },
            {
                "task_id": "97603529-2b2f-48c8-ac52-341b4f5bfd8c",
                "play_id": None,
                "task_args": {"name": None, "openstack.cloud.os_flavor_info": None},
                "spotter_noqa": [],
            },
        ]

    def test_parsing_result_playbooks_without_metadata(self) -> None:
        playbooks = [
            {
                "playbook_id": "236f197c-a773-47f6-b241-b8240f40db15",
                "plays": [
                    {
                        "play_id": "7d69009f-3cb6-4ae2-93f2-f151f924a85d",
                        "play_args": {
                            "name": None,
                            "hosts": None,
                            "collections": ["sensu.sensu_go", "ansible.builtin"],
                        },
                        "spotter_metadata": {
                            "file": "/tmp/playbook.yaml",
                            "line": 2,
                            "column": 3,
                            "start_mark_index": 6,
                            "end_mark_index": 952,
                        },
                    }
                ],
            }
        ]
        parsing_result = parsing.ParsingResult(tasks=[], playbooks=playbooks, errors=[])

        assert parsing_result.playbooks_without_metadata() == [
            {
                "playbook_id": "236f197c-a773-47f6-b241-b8240f40db15",
                "plays": [
                    {
                        "play_id": "7d69009f-3cb6-4ae2-93f2-f151f924a85d",
                        "play_args": {
                            "name": None,
                            "hosts": None,
                            "collections": ["sensu.sensu_go", "ansible.builtin"],
                        },
                    }
                ],
            }
        ]

    def test_safe_line_constructor_construct_mapping(
        self, tmp_path: Path, create_ansible_playbook: Callable[[Path], None]
    ) -> None:
        playbook_path = tmp_path / "playbook.yml"
        create_ansible_playbook(playbook_path)

        safe_line_constructor = parsing.SafeLineConstructor()
        yaml_node = ruamel.MappingNode(
            tag="tag:yaml.org,2002:map",
            value=[
                (
                    ruamel.ScalarNode(
                        tag="tag:yaml.org,2002:str",
                        value="msg",
                        start_mark=yaml_error.StringMark(
                            buffer=None, column=8, index=713, line=34, name="/tmp/playbook.yaml", pointer=0
                        ),
                        end_mark=ruamel.error.StringMark(
                            buffer=None, column=6, index=743, line=35, name="/tmp/playbook.yaml", pointer=0
                        ),
                    ),
                    ruamel.ScalarNode(
                        tag="tag:yaml.org,2002:str",
                        value="Hello from Ansible",
                        start_mark=yaml_error.StringMark(
                            buffer=None, column=8, index=713, line=34, name="/tmp/playbook.yaml", pointer=0
                        ),
                        end_mark=ruamel.error.StringMark(
                            buffer=None, column=6, index=743, line=35, name="/tmp/playbook.yaml", pointer=0
                        ),
                    ),
                )
            ],
            start_mark=yaml_error.StringMark(
                buffer=None, column=8, index=713, line=34, name="/tmp/playbook.yaml", pointer=0
            ),
            end_mark=ruamel.error.StringMark(
                buffer=None, column=6, index=743, line=35, name="/tmp/playbook.yaml", pointer=0
            ),
            flow_style=False,
        )

        assert safe_line_constructor.construct_mapping(yaml_node, ruamel.CommentedMap({})) == {
            "msg": "Hello from Ansible",
            "__meta__": {"__line__": 35, "__column__": 9, "__start_mark_index__": 713, "__end_mark_index__": 743},
        }

    def test_safe_line_constructor_construct_mapping_timestamp(self) -> None:
        safe_line_constructor = parsing.SafeLineConstructor()
        yaml_node = ruamel.MappingNode(
            tag="tag:yaml.org,2002:map",
            value=[
                (
                    ruamel.ScalarNode(
                        tag="tag:yaml.org,2002:str",
                        value="test",
                        start_mark=ruamel.error.StringMark(
                            buffer=None, column=0, index=0, line=0, name="/tmp/playbook.yaml", pointer=0
                        ),
                        end_mark=ruamel.error.StringMark(
                            buffer=None, column=0, index=5, line=0, name="/tmp/playbook.yaml", pointer=0
                        ),
                    ),
                    ruamel.ScalarNode(
                        tag="tag:yaml.org,2002:str",
                        value="2001-12-15T02:59:43.1Z",
                        start_mark=ruamel.error.StringMark(
                            buffer=None, column=0, index=6, line=0, name="/tmp/playbook.yaml", pointer=0
                        ),
                        end_mark=ruamel.error.StringMark(
                            buffer=None, column=0, index=28, line=0, name="/tmp/playbook.yaml", pointer=0
                        ),
                    ),
                )
            ],
            start_mark=ruamel.error.StringMark(
                buffer=None, column=0, index=0, line=0, name="/tmp/playbook.yaml", pointer=0
            ),
            end_mark=ruamel.error.StringMark(
                buffer=None, column=0, index=28, line=0, name="/tmp/playbook.yaml", pointer=0
            ),
            flow_style=False,
        )

        maptyp = ruamel.CommentedMap({})
        maptyp.lc.line = yaml_node.start_mark.line
        maptyp.lc.col = yaml_node.start_mark.column

        assert safe_line_constructor.construct_mapping(yaml_node, maptyp) == {
            "test": "2001-12-15T02:59:43.1Z",
            "__meta__": {"__line__": 1, "__column__": 1, "__start_mark_index__": 0, "__end_mark_index__": 28},
        }

    def test_load_yaml_file(self, tmp_path: Path, create_ansible_playbook: Callable[[Path], None]) -> None:
        playbook_path = tmp_path / "playbook.yml"
        create_ansible_playbook(playbook_path)

        assert parsing._load_yaml_file(playbook_path) == (  # pylint: disable=protected-access
            load_json("tests/unit/data/parsing/load_yaml_file.json"),
            [],
        )

    def test_load_invalid_yaml_file(self, tmp_path: Path) -> None:
        playbook_path = tmp_path / "test.yml"
        playbook_path.write_text("artifact\n  - type: ansible\n    - subtype: playbook")
        expected_error = [
            YamlErrorDetails(
                column=8,
                index=17,
                line=2,
                description="mapping values are not allowed here",
                file_path=Path(playbook_path),
            )
        ]
        loaded_yaml, parsed_error = parsing._load_yaml_file(playbook_path)  # pylint: disable=protected-access
        assert loaded_yaml is None
        assert parsed_error == expected_error

    def test_load_yaml_file_octal(self, tmp_path: Path) -> None:
        playbook_path = tmp_path / "test.yml"
        playbook_path.write_text("---\nmode: 0644")

        # pylint: disable=protected-access
        assert parsing._load_yaml_file(playbook_path) == (
            {
                "mode": 420,
                "__meta__": {"__line__": 2, "__column__": 1, "__start_mark_index__": 4, "__end_mark_index__": 14},
            },
            [],
        )

    @pytest.mark.parametrize(
        ("loaded_yaml", "expected"),
        [
            ("tests/unit/data/parsing/is_playbook_true.json", True),
            ("tests/unit/data/parsing/is_playbook_false.json", False),
        ],
    )
    def test_is_playbook(self, loaded_yaml: Any, expected: bool) -> None:
        data = load_json(loaded_yaml)
        assert parsing._is_playbook(data) == expected  # pylint: disable=protected-access

    def test_is_role(
        self,
        tmp_path: Path,
        create_ansible_playbook: Callable[[Path], None],
        create_ansible_role: Callable[[Path], None],
    ) -> None:
        role_path = tmp_path / "role"
        create_ansible_role(role_path)

        playbook_path = tmp_path / "playbook.yml"
        create_ansible_playbook(playbook_path)

        assert parsing._is_role(role_path) is True  # pylint: disable=protected-access
        assert parsing._is_role(playbook_path) is False  # pylint: disable=protected-access

    def test_is_collection(
        self,
        tmp_path: Path,
        create_ansible_role: Callable[[Path], None],
        create_ansible_collection: Callable[[Path], None],
    ) -> None:
        collection_path = tmp_path / "collection"
        create_ansible_collection(collection_path)

        role_path = tmp_path / "role"
        create_ansible_role(role_path)

        assert parsing._is_collection(collection_path) is True  # pylint: disable=protected-access
        assert parsing._is_collection(role_path) is False  # pylint: disable=protected-access

    @pytest.mark.parametrize(
        ("task", "cleaned"),
        [
            (
                {
                    "name": "Dump variables",
                    "local_action": {"module": "template", "src": "dump.j2", "dest": "/tmp/dump.all"},
                },
                {
                    "name": "Dump variables",
                    "template": {"src": "dump.j2", "dest": "/tmp/dump.all"},
                    "delegate_to": None,
                },
            ),
            (
                {
                    "name": "Restart WinRM service",
                    "action": {"module": "win_service", "name": "WinRM", "state": "restarted"},
                },
                {
                    "name": "Restart WinRM service",
                    "win_service": {"name": "WinRM", "state": "restarted"},
                },
            ),
        ],
    )
    def test_clean_action_and_local_action(self, task: dict[str, Any], cleaned: dict[str, Any]) -> None:
        parsing._clean_action_and_local_action(task)  # pylint: disable=protected-access

        assert task == cleaned

    @pytest.mark.parametrize(
        ("task", "cleaned"),
        [
            (
                "tests/unit/data/parsing/remove_deep_metadata_task.json",
                "tests/unit/data/parsing/remove_deep_metadata_task_cleaned.json",
            ),
            (
                "tests/unit/data/parsing/remove_deep_metadata_playbook.json",
                "tests/unit/data/parsing/remove_deep_metadata_playbook_cleaned.json",
            ),
        ],
    )
    def test_remove_deep_metadata(self, task: str, cleaned: str) -> None:
        task_data = load_json(task)
        cleaned_data = load_json(cleaned)
        assert parsing._remove_deep_metadata(task_data) == cleaned_data  # pylint: disable=protected-access

    def test_detect_secrets_in_file(self, tmp_path: Path) -> None:
        task_file_path = tmp_path / "task_file.yml"
        ruamel.YAML(typ="rt").dump(
            [
                {
                    "name": "Ensure that API key is present in the config file",
                    "ansible.builtin.lineinfile": {
                        "path": "/etc/test/configuration.ini",
                        "line": "API_KEY=279aa87c58354ff1844b46fd566ca513",
                    },
                },
                {
                    "name": "Create Sensu Go user",
                    "sensu.sensu_go.user": {"name": "joe", "password": "hunter2", "state": "present"},
                },
            ],
            task_file_path,
        )
        secret_values = parsing.detect_secrets_in_file(str(task_file_path))

        assert "279aa87c58354ff1844b46fd566ca513" in secret_values
        assert "hunter2" in secret_values

    @pytest.mark.parametrize(
        ("task", "cleaned"),
        [
            ("Ensure that API key is present in the config file", "Ensure that API key is present in the config file"),
            (
                {"path": "/etc/test/configuration.ini", "line": "API_KEY=279aa87c58354ff1844b46fd566ca513"},
                {"path": "/etc/test/configuration.ini", "line": None},
            ),
            ("Create Sensu Go user", "Create Sensu Go user"),
            (
                {"name": "joe", "password": "hunter2", "state": "present"},
                {"name": "joe", "password": None, "state": "present"},
            ),
        ],
    )
    def test_remove_secret_parameter_values(self, task: dict[str, Any], cleaned: dict[str, Any]) -> None:
        # pylint: disable=protected-access
        result, _ = parsing._remove_secret_parameter_values(task, ["279aa87c58354ff1844b46fd566ca513", "hunter2"])
        assert result == cleaned

    @pytest.mark.parametrize(
        ("parsed_tasks", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_tasks_true.json", True),
            ("tests/unit/data/parsing/parse_tasks_false.json", False),
        ],
    )
    def test_parse_tasks(self, parsed_tasks: str, parse_values: bool) -> None:
        yaml_path = "tests/unit/data/parsing/parse_tasks.yml"
        yaml_tasks = load_yaml(yaml_path)
        loaded_yaml_tasks = load_json("tests/unit/data/parsing/parse_tasks_loaded.json")
        assert yaml_tasks == loaded_yaml_tasks

        data = load_json(parsed_tasks)
        # pylint: disable=protected-access
        assert parsing._parse_tasks(loaded_yaml_tasks, yaml_path, parse_values) == data

    def test_parse_tasks_with_blocks(self) -> None:
        yaml_path = "tests/unit/data/parsing/parse_tasks_with_blocks.yml"
        yaml_tasks = load_yaml(yaml_path)
        loaded_yaml_tasks = load_json("tests/unit/data/parsing/parse_tasks_with_blocks_loaded.json")
        assert yaml_tasks == loaded_yaml_tasks

        data = load_json("tests/unit/data/parsing/parse_tasks_with_blocks_false.json")
        # pylint: disable=protected-access
        assert parsing._parse_tasks(loaded_yaml_tasks, yaml_path, False) == data

    @pytest.mark.parametrize(
        ("parsed_tasks", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_real_types_true.json", True),
            ("tests/unit/data/parsing/parse_real_types_false.json", False),
        ],
    )
    def test_parse_real_types(self, parsed_tasks: str, parse_values: bool) -> None:
        yaml_path = "tests/unit/data/parsing/parse_real_types.yml"
        yaml_tasks = load_yaml(yaml_path)

        data = load_json(parsed_tasks)
        # pylint: disable=protected-access
        assert parsing._parse_tasks(yaml_tasks, yaml_path, parse_values) == data

    @pytest.mark.parametrize(
        ("parsed_play", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_play_true.json", True),
            ("tests/unit/data/parsing/parse_play_false.json", False),
        ],
    )
    def test_parse_play(self, parsed_play: str, parse_values: bool) -> None:
        yaml_path = "tests/unit/data/parsing/parse_play.yml"
        yaml_play = load_yaml(yaml_path)[0]
        loaded_yaml_play = load_json("tests/unit/data/parsing/parse_play_loaded.json")
        assert yaml_play == loaded_yaml_play

        data = load_json(parsed_play)
        # pylint: disable=protected-access
        assert parsing._parse_play(loaded_yaml_play, yaml_path, parse_values) == data

    @pytest.mark.parametrize(
        ("parsed_playbook", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_playbook_true.json", True),
            ("tests/unit/data/parsing/parse_playbook_false.json", False),
        ],
    )
    def test_parse_playbook(self, parsed_playbook: str, parse_values: bool) -> None:
        yaml_path = "tests/unit/data/parsing/parse_playbook.yml"
        yaml_playbook = load_yaml(yaml_path)
        loaded_yaml_playbook = load_json("tests/unit/data/parsing/parse_playbook_loaded.json")
        assert yaml_playbook == loaded_yaml_playbook

        data = load_json(parsed_playbook)
        # pylint: disable=protected-access
        assert parsing._parse_playbook(loaded_yaml_playbook, yaml_path, parse_values) == tuple(data)

    @pytest.mark.parametrize(
        ("parsed_role", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_role_true.json", True),
            ("tests/unit/data/parsing/parse_role_false.json", False),
        ],
    )
    def test_parse_role(
        self, tmp_path: Path, create_ansible_role: Callable[[Path], None], parsed_role: str, parse_values: bool
    ) -> None:
        role_path = tmp_path / "role"
        create_ansible_role(role_path)

        data = load_json(parsed_role)
        assert parsing._parse_role(role_path, parse_values) == tuple(data)  # pylint: disable=protected-access

    @pytest.mark.parametrize(
        ("parsed_collection", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_collection_true.json", True),
            ("tests/unit/data/parsing/parse_collection_false.json", False),
        ],
    )
    def test_parse_collection(
        self,
        tmp_path: Path,
        create_ansible_collection: Callable[[Path], None],
        parsed_collection: str,
        parse_values: bool,
    ) -> None:
        collection_path = tmp_path / "collection"
        create_ansible_collection(collection_path)

        data = load_json(parsed_collection)
        # pylint: disable=protected-access
        assert parsing._parse_collection(collection_path, parse_values) == tuple(data)

    def test_parse_unknown_ansible_artifact_task_file(
        self, tmp_path: Path, create_ansible_task_file: Callable[[Path], None]
    ) -> None:
        task_file_path = tmp_path / "task_file.yml"
        create_ansible_task_file(task_file_path)
        parsed_tasks, parsed_playbooks, parsed_errors = parsing.parse_unknown_ansible_artifact(task_file_path)

        assert len(parsed_tasks) == 3
        assert len(parsed_playbooks) == 0
        assert len(parsed_errors) == 0

    def test_parse_unknown_ansible_artifact_playbook(
        self, tmp_path: Path, create_ansible_playbook: Callable[[Path], None]
    ) -> None:
        playbook_path = tmp_path / "playbook.yml"
        create_ansible_playbook(playbook_path)
        parsed_tasks, parsed_playbooks, parsed_errors = parsing.parse_unknown_ansible_artifact(playbook_path)

        assert len(parsed_tasks) == 5
        assert len(parsed_playbooks) == 1
        assert len(parsed_errors) == 0

    def test_parse_unknown_ansible_artifact_task_role(
        self, tmp_path: Path, create_ansible_role: Callable[[Path], None]
    ) -> None:
        role_path = tmp_path / "role"
        create_ansible_role(role_path)
        parsed_tasks, parsed_playbooks, parsed_errors = parsing.parse_unknown_ansible_artifact(role_path)

        assert len(parsed_tasks) == 3
        assert len(parsed_playbooks) == 0
        assert len(parsed_errors) == 0

    def test_parse_unknown_ansible_artifact_task_collection(
        self, tmp_path: Path, create_ansible_collection: Callable[[Path], None]
    ) -> None:
        collection_path = tmp_path / "collection"
        create_ansible_collection(collection_path)
        parsed_tasks, parsed_playbooks, parsed_errors = parsing.parse_unknown_ansible_artifact(collection_path)

        assert len(parsed_tasks) == 16
        assert len(parsed_playbooks) == 2
        assert len(parsed_errors) == 0

    def test_parse_ansible_artifacts_task_file(
        self, tmp_path: Path, create_ansible_task_file: Callable[[Path], None]
    ) -> None:
        task_file_path = tmp_path / "task_file.yml"
        create_ansible_task_file(task_file_path)
        parsing_result = parsing.parse_ansible_artifacts([task_file_path])

        assert len(parsing_result.tasks) == 3
        assert len(parsing_result.playbooks) == 0

    def test_parse_ansible_artifacts_playbook(
        self, tmp_path: Path, create_ansible_playbook: Callable[[Path], None]
    ) -> None:
        playbook_path = tmp_path / "playbook.yml"
        create_ansible_playbook(playbook_path)
        parsing_result = parsing.parse_ansible_artifacts([playbook_path])

        assert len(parsing_result.tasks) == 5
        assert len(parsing_result.playbooks) == 1

    def test_parse_ansible_artifacts_role(self, tmp_path: Path, create_ansible_role: Callable[[Path], None]) -> None:
        role_path = tmp_path / "role"
        create_ansible_role(role_path)
        parsing_result = parsing.parse_ansible_artifacts([role_path])

        assert len(parsing_result.tasks) == 3
        assert len(parsing_result.playbooks) == 0

    def test_parse_ansible_artifacts_collection(
        self, tmp_path: Path, create_ansible_collection: Callable[[Path], None]
    ) -> None:
        collection_path = tmp_path / "collection"
        create_ansible_collection(collection_path)
        parsing_result = parsing.parse_ansible_artifacts([collection_path])

        assert len(parsing_result.tasks) == 16
        assert len(parsing_result.playbooks) == 2

    def test_check_type_other(self) -> None:
        element = {"check_type": "other"}
        with mock.patch("builtins.print") as print_mock:
            check_type = CheckType.from_string(element.get("check_type", ""))

        assert check_type == CheckType.OTHER
        print_mock.assert_not_called()

    def test_check_type_fail(self) -> None:
        element = {"check_type": "..."}
        with mock.patch("builtins.print") as print_mock:
            check_type = CheckType.from_string(element.get("check_type", ""))

        assert check_type == CheckType.OTHER
        print_mock.assert_called_once_with(
            "Warning: nonexistent check result type: ..., "
            "valid values are: ['task', 'play', 'requirements', 'ansible_cfg', 'other'].",
            file=sys.stderr,
        )

    def test_safe_line_constructor(self) -> None:
        yaml = ruamel.YAML(typ="rt")
        yaml.Constructor = parsing.SafeLineConstructor
        yaml.version = (1, 1)

        with open("tests/unit/data/parsing/safelineloader.yml", "r", encoding="utf-8") as f:
            f.seek(0)
            as_yml = yaml.load(f)
        task = as_yml[0]["tasks"][0]
        start_index = task["__meta__"]["__start_mark_index__"]
        assert start_index == 57

        with open("tests/unit/data/rewriting/input/inline.yml", "r", encoding="utf-8") as f:
            f.seek(0)
            as_yml = yaml.load(f)
        start_index = as_yml[0]["__meta__"]["__start_mark_index__"]
        assert start_index == 8

    @pytest.mark.parametrize(
        ("noqa_comment", "parsed_comment"),
        [
            ("", []),
            ("test123", []),
            ("# noqaa: H001", []),
            ("# noqa: H001", [noqa_comments.SpotterNoqa(event="H001")]),
            ("#  noqa: H001, E003", [noqa_comments.SpotterNoqa(event="H001"), noqa_comments.SpotterNoqa(event="E003")]),
            ("#noqa:H001 E003", [noqa_comments.SpotterNoqa(event="H001"), noqa_comments.SpotterNoqa(event="E003")]),
            (
                "# noqa:  H1900[fqcn=sensu.sensu_go.user]",
                [noqa_comments.SpotterNoqa(event="H1900", fqcn="sensu.sensu_go.user")],
            ),
            (
                "# noqa: E001[fqcn=ansible.builtin.uri,subevent_code=23456], "
                "W040[fqcn=ansible.builtin.copy,subevent_code=1234]",
                [
                    noqa_comments.SpotterNoqa(event="E001", fqcn="ansible.builtin.uri", subevent_code="23456"),
                    noqa_comments.SpotterNoqa(event="W040", fqcn="ansible.builtin.copy", subevent_code="1234"),
                ],
            ),
        ],
    )
    def test_parse_noqa_comment(self, noqa_comment: str, parsed_comment: List[noqa_comments.SpotterNoqa]) -> None:
        assert noqa_comments.SpotterNoqa.parse_noqa_comment(noqa_comment) == parsed_comment

    @pytest.mark.parametrize(
        ("noqa_comment", "parsed_comment"),
        [
            ("", []),
            ("test123", []),
            ("H001", [noqa_comments.SpotterNoqa(event="H001")]),
            ("H001, E003", [noqa_comments.SpotterNoqa(event="H001"), noqa_comments.SpotterNoqa(event="E003")]),
            ("H001 E003", [noqa_comments.SpotterNoqa(event="H001"), noqa_comments.SpotterNoqa(event="E003")]),
            ("H1900[fqcn=sensu.sensu_go.user]", [noqa_comments.SpotterNoqa(event="H1900", fqcn="sensu.sensu_go.user")]),
            (
                "E001[fqcn=ansible.builtin.uri,subevent_code=23456], "
                "W040[fqcn=ansible.builtin.copy,subevent_code=1234]",
                [
                    noqa_comments.SpotterNoqa(event="E001", fqcn="ansible.builtin.uri", subevent_code="23456"),
                    noqa_comments.SpotterNoqa(event="W040", fqcn="ansible.builtin.copy", subevent_code="1234"),
                ],
            ),
        ],
    )
    def test_parse_noqa_comment_skip_noqa_regex(
        self, noqa_comment: str, parsed_comment: List[noqa_comments.SpotterNoqa]
    ) -> None:
        assert noqa_comments.SpotterNoqa.parse_noqa_comment(noqa_comment, use_noqa_regex=False) == parsed_comment

    @pytest.mark.parametrize(
        ("noqa_comment", "parsed_comment_part"),
        [
            ("", None),
            ("test123", None),
            ("fqcn=sensu.sensu_go.user", "sensu.sensu_go.user"),
            ("[fqcn=sensu.sensu_go.user]", "sensu.sensu_go.user"),
        ],
    )
    def test_parse_fqcn_comment_part(self, noqa_comment: str, parsed_comment_part: Optional[str]) -> None:
        # pylint: disable=protected-access
        assert noqa_comments.SpotterNoqa._parse_fqcn(noqa_comment) == parsed_comment_part

    @pytest.mark.parametrize(
        ("noqa_comment", "parsed_comment_part"),
        [("", None), ("test123", None), ("subevent_code=12345", "12345"), ("[subevent_code=12345]", "12345")],
    )
    def test_parse_subevent_code_comment_part(self, noqa_comment: str, parsed_comment_part: Optional[str]) -> None:
        # pylint: disable=protected-access
        assert noqa_comments.SpotterNoqa._parse_subevent_code(noqa_comment) == parsed_comment_part

    @pytest.mark.parametrize(
        ("comment_list", "parsed_comment"),
        [
            ([], []),
            ([None, None, ruamel.CommentToken("\n\n", column=1), None], []),
            (
                [None, None, ruamel.CommentToken("\n        # noqa: H500\n", column=1), None],
                [noqa_comments.SpotterNoqa(event="H500")],
            ),
            (
                [
                    ruamel.CommentToken(
                        "\n\n    # noqa: H1900[fqcn=community.crypto.x509_certificate], "
                        "E601[fqcn=community.crypto.x509_certificate]\n    # wohoooo\n",
                        column=1,
                    ),
                    None,
                ],
                [
                    noqa_comments.SpotterNoqa(event="H1900", fqcn="community.crypto.x509_certificate"),
                    noqa_comments.SpotterNoqa(event="E601", fqcn="community.crypto.x509_certificate"),
                ],
            ),
        ],
    )
    def test_construct_noqa_from_comment_list(
        self, comment_list: List[Any], parsed_comment: List[noqa_comments.SpotterNoqa]
    ) -> None:
        # pylint: disable=protected-access
        assert noqa_comments._construct_noqa_from_comment_list(comment_list) == parsed_comment

    def test_construct_noqa_from_commented_item_map(self) -> None:
        commented_map = ruamel.CommentedMap()
        commented_map["test"] = "test123"
        commented_map.yaml_add_eol_comment("# noqa: H1900[fqcn=community.crypto.x509_certificate]", "test", column=0)
        # pylint: disable=protected-access
        assert noqa_comments._construct_noqa_from_commented_item(commented_map) == [
            noqa_comments.SpotterNoqa(event="H1900", fqcn="community.crypto.x509_certificate")
        ]

    def test_construct_noqa_from_commented_item_sequence(self) -> None:
        commented_seq = ruamel.CommentedSeq()
        commented_seq.append("test")
        commented_seq.yaml_add_eol_comment("# noqa: H1900[fqcn=community.crypto.x509_certificate]", "test", column=0)
        # pylint: disable=protected-access
        assert noqa_comments._construct_noqa_from_commented_item(commented_seq) == [
            noqa_comments.SpotterNoqa(event="H1900", fqcn="community.crypto.x509_certificate")
        ]

    def test_construct_noqa_from_dict_recursive(self) -> None:
        commented_map = ruamel.CommentedMap()
        commented_map["test"] = "test123"
        commented_map.yaml_add_eol_comment("# noqa: H1900[fqcn=community.crypto.x509_certificate]", "test", column=0)
        # pylint: disable=protected-access
        assert noqa_comments._construct_noqa_from_dict_recursive({"test": {"test123": commented_map}}) == [
            noqa_comments.SpotterNoqa(event="H1900", fqcn="community.crypto.x509_certificate")
        ]

    def test_match_comments_with_task(self) -> None:
        commented_map1 = ruamel.CommentedMap()
        commented_map1["test"] = "test123"
        commented_map1.yaml_add_eol_comment("# noqa: H1900[fqcn=community.crypto.x509_certificate]", "test", column=0)
        commented_map2 = ruamel.CommentedMap()
        commented_map2["another_test"] = "test321"

        noqa_comments.match_comments_with_task(commented_map1)
        assert commented_map1 == {
            "test": "test123",
            "__noqa__": [
                noqa_comments.SpotterNoqa(event="H1900", subevent_code=None, fqcn="community.crypto.x509_certificate")
            ],
        }

        noqa_comments.match_comments_with_task(commented_map2)
        assert commented_map2 == {"another_test": "test321", "__noqa__": []}

    @pytest.mark.parametrize(
        ("parsed_playbook", "parse_values"),
        [
            ("tests/unit/data/parsing/parse_playbook_comments_true.json", True),
            ("tests/unit/data/parsing/parse_playbook_comments_false.json", False),
        ],
    )
    def test_parse_playbook_with_comments(self, parsed_playbook: str, parse_values: bool) -> None:
        yaml_path = "tests/unit/data/parsing/parse_playbook_comments.yml"
        yaml_playbook = load_yaml(yaml_path)
        loaded_yaml_playbook = load_json("tests/unit/data/parsing/parse_playbook_comments_loaded.json")
        assert yaml_playbook == loaded_yaml_playbook

        data = load_json(parsed_playbook)
        # pylint: disable=protected-access
        assert parsing._parse_playbook(yaml_playbook, yaml_path, parse_values) == tuple(data)

    def test_parse_playbook_with_lint_comments(self) -> None:
        yaml_path = "tests/unit/data/parsing/parse_playbook_lint_comments.yml"
        yaml_playbook = load_yaml(yaml_path)
        loaded_yaml_playbook = load_json("tests/unit/data/parsing/parse_playbook_lint_comments_loaded.json")
        assert yaml_playbook == loaded_yaml_playbook

        data = load_json("tests/unit/data/parsing/parse_playbook_lint_comments_false.json")
        # pylint: disable=protected-access
        assert parsing._parse_playbook(yaml_playbook, yaml_path, False) == tuple(data)

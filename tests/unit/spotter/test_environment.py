import json
from pathlib import Path
from unittest import mock

import pytest
import ruamel.yaml as ruamel

from spotter.library.environment import EnvironmentAnsibleVersion, Environment


class TestEnvironment:
    # pylint: disable=too-many-public-methods

    def test_create_environment_ansible_version_minimal(self) -> None:
        environment_ansible_version = EnvironmentAnsibleVersion()

        assert environment_ansible_version.ansible_core is None
        assert environment_ansible_version.ansible_base is None
        assert environment_ansible_version.ansible is None

    def test_create_environment_ansible_version(self) -> None:
        environment_ansible_version = EnvironmentAnsibleVersion(
            ansible_core="2.14.0", ansible_base="2.10.17", ansible="[core 2.14.0]"
        )

        assert environment_ansible_version.ansible_core == "2.14.0"
        assert environment_ansible_version.ansible_base == "2.10.17"
        assert environment_ansible_version.ansible == "[core 2.14.0]"

    def test_create_minimal(self) -> None:
        environment = Environment()

        assert environment.ansible_version is None
        assert environment.python_version is None
        assert environment.installed_collections is None
        assert environment.ansible_config is None
        assert environment.galaxy_yml is None
        assert environment.collection_requirements is None
        assert environment.cli_scan_args is None

    def test_create(self) -> None:
        environment = Environment(
            python_version="3.11",
            ansible_version=EnvironmentAnsibleVersion(
                ansible_core="2.14.0", ansible_base="2.10.17", ansible="[core 2.14.0]"
            ),
            installed_collections=[
                {"fqcn": "community.general", "version": "4.6.1"},
                {"fqcn": "community.kubernetes", "version": "1.1.1"},
                {"fqcn": "sensu.sensu_go", "version": "1.13.0"},
            ],
            ansible_config={
                "DEFAULT_CLICONF_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/cliconf', "
                "'/usr/share/ansible/plugins/cliconf']",
                "DEFAULT_CONNECTION_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/connection', "
                "'/usr/share/ansible/plugins/connection']",
                "DEFAULT_DEBUG(/home/user/ansible.cfg)": "False",
                "DEFAULT_EXECUTABLE(/home/user/ansible.cfg)": "/bin/sh",
            },
            galaxy_yml={
                "namespace": "steampunk_spotter",
                "name": "example",
                "readme": "README.md",
                "authors": "Steampunk Spotter",
                "description": "Steampunk Spotter example for galaxy.yml",
                "license": "GPL-3.0-or-later",
                "tags": ["example", "steampunk", "spotter"],
                "dependencies": {},
                "build_ignore": [".gitignore", ".env", ".vscode"],
            },
            collection_requirements={
                "collections": [
                    {"name": "sensu.sensu_go"},
                    {"name": "openstack.cloud", "version": "1.10.0"},
                    {
                        "name": "steampunk_spotter.example",
                        "version": "10.10.10",
                        "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                        "source": "https://galaxy.ansible.com",
                        "type": "galaxy",
                    },
                ],
                "roles": [
                    {"src": "yatesr.timezone"},
                    {"name": "0x0i.systemd", "version": "v0.3.5"},
                    {
                        "src": "https://github.com/bennojoy/nginx",
                        "scm": "git",
                        "version": "master",
                        "name": "nginx_role",
                    },
                ],
            },
            cli_scan_args={"upload_metadata": True, "rewrite": True, "display_level": "hint"},
        )

        assert environment.python_version == "3.11"
        assert environment.ansible_version.ansible_core == "2.14.0"  # type: ignore
        assert environment.ansible_version.ansible_base == "2.10.17"  # type: ignore
        assert environment.ansible_version.ansible == "[core 2.14.0]"  # type: ignore
        assert environment.installed_collections == [
            {"fqcn": "community.general", "version": "4.6.1"},
            {"fqcn": "community.kubernetes", "version": "1.1.1"},
            {"fqcn": "sensu.sensu_go", "version": "1.13.0"},
        ]
        assert environment.ansible_config == {
            "DEFAULT_CLICONF_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/cliconf', "
            "'/usr/share/ansible/plugins/cliconf']",
            "DEFAULT_CONNECTION_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/connection', "
            "'/usr/share/ansible/plugins/connection']",
            "DEFAULT_DEBUG(/home/user/ansible.cfg)": "False",
            "DEFAULT_EXECUTABLE(/home/user/ansible.cfg)": "/bin/sh",
        }
        assert environment.galaxy_yml == {
            "namespace": "steampunk_spotter",
            "name": "example",
            "readme": "README.md",
            "authors": "Steampunk Spotter",
            "description": "Steampunk Spotter example for galaxy.yml",
            "license": "GPL-3.0-or-later",
            "tags": ["example", "steampunk", "spotter"],
            "dependencies": {},
            "build_ignore": [".gitignore", ".env", ".vscode"],
        }
        assert environment.collection_requirements == {
            "collections": [
                {"name": "sensu.sensu_go"},
                {"name": "openstack.cloud", "version": "1.10.0"},
                {
                    "name": "steampunk_spotter.example",
                    "version": "10.10.10",
                    "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                    "source": "https://galaxy.ansible.com",
                    "type": "galaxy",
                },
            ],
            "roles": [
                {"src": "yatesr.timezone"},
                {"name": "0x0i.systemd", "version": "v0.3.5"},
                {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
            ],
        }
        assert environment.cli_scan_args == {"upload_metadata": True, "rewrite": True, "display_level": "hint"}

    def test_from_config_file_json(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / "config.json"
        config_file_path.write_text(json.dumps({"ansible_version": "2.14"}))
        environment = Environment.from_config_file(config_file_path)

        assert environment.ansible_version.ansible_core == "2.14"  # type: ignore

    def test_from_config_file_yaml(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / "config.yml"
        ruamel.YAML(typ="rt").dump({"ansible_version": "2.14"}, config_file_path)
        environment = Environment.from_config_file(config_file_path)

        assert environment.ansible_version.ansible_core == "2.14"  # type: ignore

    def test_from_config_file_empty(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / "config.yml"
        config_file_path.write_text("")
        environment = Environment.from_config_file(config_file_path)

        assert environment.ansible_version is None

    def test_from_config_file_nonexistent_option(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / "config.yml"
        ruamel.YAML(typ="rt").dump({"ansible_distribution": "2.14"}, config_file_path)

        with pytest.raises(SystemExit) as exception_info:
            Environment.from_config_file(config_file_path)

        assert exception_info.value.code == 2

    def test_from_config_file_invalid(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / "config.yml"
        config_file_path.write_text("invalid invalid invalid")

        with pytest.raises(SystemExit) as exception_info:
            Environment.from_config_file(config_file_path)

        assert exception_info.value.code == 2

    def test_from_project_config_file_json(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / ".spotter.json"
        config_file_path.write_text(json.dumps({"ansible_version": "2.14"}))

        with mock.patch.object(Path, "cwd", return_value=tmp_path):
            environment = Environment.from_project_configuration_file()

        assert environment.ansible_version.ansible_core == "2.14"  # type: ignore

    def test_from_project_config_file_yml(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / ".spotter.yml"
        ruamel.YAML(typ="rt").dump({"ansible_version": "2.14"}, config_file_path)

        with mock.patch.object(Path, "cwd", return_value=tmp_path):
            environment = Environment.from_project_configuration_file()

        assert environment.ansible_version.ansible_core == "2.14"  # type: ignore

    def test_from_project_config_file_yaml(self, tmp_path: Path) -> None:
        config_file_path = tmp_path / ".spotter.yaml"
        ruamel.YAML(typ="rt").dump({"ansible_version": "2.14"}, config_file_path)

        with mock.patch.object(Path, "cwd", return_value=tmp_path):
            environment = Environment.from_project_configuration_file()

        assert environment.ansible_version.ansible_core == "2.14"  # type: ignore

    def test_from_local_discovery_get_python_version(self, tmp_path: Path) -> None:
        python_version = "3.11"
        with mock.patch.object(Environment, "_get_python_version", return_value=python_version):
            environment = Environment.from_local_discovery([tmp_path])

        assert environment.python_version == python_version

    def test_from_local_discovery_get_ansible_core_python_version(self, tmp_path: Path) -> None:
        ansible_core_version = "2.14.0"
        with mock.patch.object(Environment, "_get_ansible_core_python_version", return_value=ansible_core_version):
            environment = Environment.from_local_discovery([tmp_path])

        assert environment.ansible_version.ansible_core == ansible_core_version  # type: ignore

    def test_from_local_discovery_get_ansible_base_python_version(self, tmp_path: Path) -> None:
        ansible_base_version = "2.10.17"
        with mock.patch.object(Environment, "_get_ansible_base_python_version", return_value=ansible_base_version):
            environment = Environment.from_local_discovery([tmp_path])

        assert environment.ansible_version.ansible_base == ansible_base_version  # type: ignore

    def test_from_local_discovery_get_ansible_version(self, tmp_path: Path) -> None:
        ansible_version = "7.7.0"
        with mock.patch.object(Environment, "_get_ansible_version", return_value=ansible_version):
            environment = Environment.from_local_discovery([tmp_path])

        assert environment.ansible_version.ansible == ansible_version  # type: ignore

    def test_from_local_discovery_get_installed_ansible_collections(self, tmp_path: Path) -> None:
        installed_collections = [
            {"fqcn": "community.general", "version": "4.6.1"},
            {"fqcn": "community.kubernetes", "version": "1.1.1"},
            {"fqcn": "sensu.sensu_go", "version": "1.13.0"},
        ]
        with mock.patch.object(Environment, "_get_installed_ansible_collections", return_value=installed_collections):
            environment = Environment.from_local_discovery([tmp_path])

        assert environment.installed_collections == installed_collections

    def test_from_local_discovery_get_ansible_config(self, tmp_path: Path) -> None:
        ansible_config_only_changed = {
            "DEFAULT_CLICONF_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/cliconf', "
            "'/usr/share/ansible/plugins/cliconf']",
            "DEFAULT_CONNECTION_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/connection', "
            "'/usr/share/ansible/plugins/connection']",
            "DEFAULT_DEBUG(/home/user/ansible.cfg)": "False",
            "DEFAULT_EXECUTABLE(/home/user/ansible.cfg)": "/bin/sh",
        }
        with mock.patch.object(Environment, "_get_ansible_config", return_value=ansible_config_only_changed):
            environment = Environment.from_local_discovery([tmp_path])

        assert environment.ansible_config == ansible_config_only_changed

    def test_from_local_discovery_get_galaxy_yml(self, tmp_path: Path) -> None:
        galaxy_yml_path = tmp_path / "galaxy.yml"
        galaxy_yml_content = {
            "namespace": "steampunk_spotter",
            "name": "example",
            "readme": "README.md",
            "authors": "XLAB Steampunk",
            "description": "Steampunk Spotter example for galaxy.yml.",
            "license": "GPL-3.0-or-later",
            "tags": ["example", "steampunk", "spotter"],
            "dependencies": {},
            "build_ignore": [".gitignore", ".env", ".vscode"],
        }
        ruamel.YAML(typ="rt").dump(galaxy_yml_content, galaxy_yml_path)

        assert Environment._get_galaxy_yml(tmp_path) == galaxy_yml_content  # pylint: disable=protected-access
        assert Environment.from_local_discovery([tmp_path]).galaxy_yml == galaxy_yml_content

    def test_from_local_discovery_validate_collection_requirements(self, tmp_path: Path) -> None:
        requirements_yml_path = tmp_path / "requirements.yml"
        collection_requirements = [
            "sensu.sensu_go",
            {"name": "openstack.cloud", "version": "1.10.0"},
            {
                "name": "steampunk_spotter.example",
                "version": "10.10.10",
                "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                "source": "https://galaxy.ansible.com",
                "type": "galaxy",
            },
        ]

        collection_requirements_parsed = [
            {"name": "sensu.sensu_go"},
            {"name": "openstack.cloud", "version": "1.10.0"},
            {
                "name": "steampunk_spotter.example",
                "version": "10.10.10",
                "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                "source": "https://galaxy.ansible.com",
                "type": "galaxy",
            },
        ]
        # pylint: disable=protected-access
        assert (
            Environment._validate_collection_requirements(collection_requirements, requirements_yml_path)
            == collection_requirements_parsed
        )

    def test_from_local_discovery_validate_role_requirements(self, tmp_path: Path) -> None:
        requirements_yml_path = tmp_path / "requirements.yml"
        role_requirements = [
            "yatesr.timezone",
            {"name": "0x0i.systemd", "version": "v0.3.5"},
            {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
        ]

        role_requirements_parsed = [
            {"src": "yatesr.timezone"},
            {"name": "0x0i.systemd", "version": "v0.3.5"},
            {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
        ]
        # pylint: disable=protected-access
        assert (
            Environment._validate_role_requirements(role_requirements, requirements_yml_path)
            == role_requirements_parsed
        )

    def test_from_local_discovery_get_requirements(self, tmp_path: Path) -> None:
        requirements_yml_path = tmp_path / "requirements.yml"
        requirements_yml_content = {
            "collections": [
                "sensu.sensu_go",
                {"name": "openstack.cloud", "version": "1.10.0"},
                {
                    "name": "steampunk_spotter.example",
                    "version": "10.10.10",
                    "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                    "source": "https://galaxy.ansible.com",
                    "type": "galaxy",
                },
            ],
            "roles": [
                "yatesr.timezone",
                {"name": "0x0i.systemd", "version": "v0.3.5"},
                {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
            ],
        }
        ruamel.YAML(typ="rt").dump(requirements_yml_content, requirements_yml_path)

        requirements_yml_parsed = {
            "collections": [
                {"name": "sensu.sensu_go"},
                {"name": "openstack.cloud", "version": "1.10.0"},
                {
                    "name": "steampunk_spotter.example",
                    "version": "10.10.10",
                    "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                    "source": "https://galaxy.ansible.com",
                    "type": "galaxy",
                },
            ],
            "roles": [
                {"src": "yatesr.timezone"},
                {"name": "0x0i.systemd", "version": "v0.3.5"},
                {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
            ],
        }
        # pylint: disable=protected-access
        assert Environment._get_requirements(tmp_path) == requirements_yml_parsed
        assert Environment.from_local_discovery([tmp_path]).collection_requirements == requirements_yml_parsed

    def test_from_local_discovery_get_requirements_just_roles(self, tmp_path: Path) -> None:
        requirements_yml_path = tmp_path / "requirements.yml"

        requirements_yml_content = [
            "yatesr.timezone",
            {"name": "0x0i.systemd", "version": "v0.3.5"},
            {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
        ]
        ruamel.YAML(typ="rt").dump(requirements_yml_content, requirements_yml_path)

        requirements_yml_parsed = {
            "roles": [
                {"src": "yatesr.timezone"},
                {"name": "0x0i.systemd", "version": "v0.3.5"},
                {"src": "https://github.com/bennojoy/nginx", "scm": "git", "version": "master", "name": "nginx_role"},
            ]
        }
        # pylint: disable=protected-access
        assert Environment._get_requirements(tmp_path) == requirements_yml_parsed
        assert Environment.from_local_discovery([tmp_path]).collection_requirements == requirements_yml_parsed

    def test_from_local_discovery_get_requirements_empty(self, tmp_path: Path) -> None:
        requirements_yml_path = tmp_path / "requirements.yml"
        ruamel.YAML(typ="rt").dump({}, requirements_yml_path)
        # pylint: disable=protected-access
        assert Environment._get_requirements(tmp_path) == {}
        assert not Environment.from_local_discovery([tmp_path]).collection_requirements

    def test_combine(self) -> None:
        environment1 = Environment(
            python_version="3.10",
            ansible_version=EnvironmentAnsibleVersion(
                ansible_core="2.14.0", ansible_base="2.10.17", ansible="[core 2.14.0]"
            ),
            installed_collections=[
                {"fqcn": "community.general", "version": "6.1.0"},
                {"fqcn": "community.kubernetes", "version": "1.1.1"},
            ],
            ansible_config={
                "DEFAULT_CLICONF_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/cliconf', "
                "'/usr/share/ansible/plugins/cliconf']",
                "DEFAULT_CONNECTION_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/connection', "
                "'/usr/share/ansible/plugins/connection']",
                "DEFAULT_DEBUG(/home/user/ansible.cfg)": "False",
                "DEFAULT_EXECUTABLE(/home/user/ansible.cfg)": "/bin/sh",
            },
            collection_requirements={
                "roles": [{"src": "yatesr.timezone"}, {"name": "0x0i.systemd", "version": "v0.3.5"}]
            },
            cli_scan_args={"upload_metadata": True, "display_level": "hint"},
        )

        environment2 = Environment(
            python_version="3.11",
            ansible_version=EnvironmentAnsibleVersion(
                ansible_core="2.14.1", ansible_base=None, ansible="[core 2.14.1]"
            ),
            installed_collections=[
                {"fqcn": "community.kubernetes", "version": "1.1.1"},
                {"fqcn": "sensu.sensu_go", "version": "1.13.2"},
            ],
            galaxy_yml={
                "namespace": "steampunk_spotter",
                "name": "example",
                "readme": "README.md",
                "authors": "Steampunk Spotter",
                "description": "Steampunk Spotter example for galaxy.yml",
                "license": "GPL-3.0-or-later",
                "tags": ["example", "steampunk", "spotter"],
                "dependencies": {},
                "build_ignore": [".gitignore", ".env", ".vscode"],
            },
            collection_requirements={
                "collections": [
                    {"name": "sensu.sensu_go"},
                    {"name": "openstack.cloud", "version": "1.10.0"},
                    {
                        "name": "steampunk_spotter.example",
                        "version": "10.10.10",
                        "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                        "source": "https://galaxy.ansible.com",
                        "type": "galaxy",
                    },
                ],
                "roles": [
                    {"src": "yatesr.timezone"},
                    {
                        "src": "https://github.com/bennojoy/nginx",
                        "scm": "git",
                        "version": "master",
                        "name": "nginx_role",
                    },
                ],
            },
            cli_scan_args={"rewrite": True, "display_level": "error", "version": "3.0.0", "origin": "cli"},
        )

        environment_combined = Environment(
            python_version="3.11",
            ansible_version=EnvironmentAnsibleVersion(
                ansible_core="2.14.1", ansible_base=None, ansible="[core 2.14.1]"
            ),
            installed_collections=[
                {"fqcn": "community.kubernetes", "version": "1.1.1"},
                {"fqcn": "sensu.sensu_go", "version": "1.13.2"},
            ],
            ansible_config={
                "DEFAULT_CLICONF_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/cliconf', "
                "'/usr/share/ansible/plugins/cliconf']",
                "DEFAULT_CONNECTION_PLUGIN_PATH(/home/user/ansible.cfg)": "['/home/user/.ansible/plugins/connection', "
                "'/usr/share/ansible/plugins/connection']",
                "DEFAULT_DEBUG(/home/user/ansible.cfg)": "False",
                "DEFAULT_EXECUTABLE(/home/user/ansible.cfg)": "/bin/sh",
            },
            galaxy_yml={
                "namespace": "steampunk_spotter",
                "name": "example",
                "readme": "README.md",
                "authors": "Steampunk Spotter",
                "description": "Steampunk Spotter example for galaxy.yml",
                "license": "GPL-3.0-or-later",
                "tags": ["example", "steampunk", "spotter"],
                "dependencies": {},
                "build_ignore": [".gitignore", ".env", ".vscode"],
            },
            collection_requirements={
                "collections": [
                    {"name": "sensu.sensu_go"},
                    {"name": "openstack.cloud", "version": "1.10.0"},
                    {
                        "name": "steampunk_spotter.example",
                        "version": "10.10.10",
                        "signatures": ["https://example.com/signature.asc", "file:///local/path/signature.asc"],
                        "source": "https://galaxy.ansible.com",
                        "type": "galaxy",
                    },
                ],
                "roles": [
                    {"src": "yatesr.timezone"},
                    {
                        "src": "https://github.com/bennojoy/nginx",
                        "scm": "git",
                        "version": "master",
                        "name": "nginx_role",
                    },
                ],
            },
            cli_scan_args={"rewrite": True, "display_level": "error", "version": "3.0.0", "origin": "cli"},
        )

        assert environment1.combine(environment2) == environment_combined

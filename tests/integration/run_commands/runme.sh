#!/bin/bash

set -euo pipefail

echo "Running run_commands ..."
spotter_executable="$1"
storage_path="$(mktemp -d)"
common_args="--storage-path ${storage_path} --endpoint http://localhost:8080 --timeout 10 --no-color --debug"

echo "Starting web server ..."
python3 run_web_server.py 8080 &
serverPID=$!

echo "Getting Spotter CLI version ..."
$spotter_executable --version
test $? = 0

echo "Running login ..."
$spotter_executable $common_args --token example_api_token login
test $? = 0

echo "Running scan ..."
set +euo pipefail
$spotter_executable $common_args scan --import-payload data/payload.json
test $? = 1
set -euo pipefail

echo "Running scan with rewrite ..."
cp data/playbook.yml playbook.yml
set +euo pipefail
$spotter_executable $common_args scan --rewrite --import-payload data/payload.json
test $? = 1
set -euo pipefail
test -f requirements.yml
test $? = 0
rm playbook.yml requirements.yml

echo "Running scan with export ..."
scan_output_path="$(mktemp).json"
$spotter_executable $common_args scan \
                    --import-payload data/payload.json --format json --output "$scan_output_path" || true
test $? = 0
diff <(jq '.summary.scan_time = 0.003' "$scan_output_path") "data/scan_output.json"
test $? = 0
rm -rf "$scan_output_path"

echo "Running scan with JUnit XML report ..."
set +euo pipefail
$spotter_executable $common_args scan --junit-xml report.xml --import-payload data/payload.json
test $? = 1
set -euo pipefail
test -f report.xml
test $? = 0
rm report.xml

echo "Running scan with Sarif report ..."
set +euo pipefail
$spotter_executable $common_args scan --sarif report.sarif --import-payload data/payload.json
test $? = 1
set -euo pipefail
test -f report.sarif
test $? = 0
rm report.sarif

echo "Running policies set ..."
$spotter_executable $common_args policies set data/policies.rego
test $? = 0

echo "Running policies clear ..."
$spotter_executable $common_args policies clear
test $? = 0

echo "Running config set..."
$spotter_executable $common_args config set data/org-config.json
test $? = 0

echo "Running config get..."
$spotter_executable $common_args config get
test $? = 0

echo "Running config clear ..."
$spotter_executable $common_args config clear
test $? = 0

echo "Running suggest ..."
$spotter_executable $common_args suggest --num-results 3 "Create a new EC2 instance"
test $? = 0

echo "Running logout ..."
$spotter_executable $common_args logout
test $? = 0

echo "Stopping web server and cleaning up ..."
rm -rf "$storage_path"
kill $serverPID
echo "Done!"

package Spotter

SpotterPolicy[result] {
	task := input.tasks[i]
	task_args := task.task_args
	regex.match("^[A-Z].*", task_args.name) == false

    result := {
		"correlation_id": task.task_id,
		"check_type": "TASK",
		"message": "Name value should start with uppercase letter"
	}
}

SpotterPolicy[result] {
	task := input.tasks[i]
	task_args := task.task_args
	regex.match("\\.$", task_args.name) == false
    
    result := {
		"correlation_id": task.task_id,
		"check_type": "TASK",
		"message": "Name value end with a Dot"
	}

}

SpotterPolicy[result] {
	task := input.tasks[i]
	task_args := task.task_args
	task_args.name == false

    result := {
		"correlation_id": task.task_id,
		"check_type": "TASK",
		"message": "Name has to include string value"
	}

}

import json
import sys
from typing import Any

from pytest_httpserver import HTTPServer


def load_json(filename: str) -> Any:
    with open(filename, "r", encoding="utf-8") as f:
        result = json.load(f)
        return result


def get_port() -> int:
    if len(sys.argv) == 1:
        return 8080
    if len(sys.argv) > 2:
        print("Too many arguments were supplied!")
        sys.exit(1)

    try:
        port = int(sys.argv[1])
        if 1 > port > 65535:
            print(f"{port} is not a valid port number.")
            sys.exit(1)
        return port
    except ValueError:
        print("Argument should be a valid integer!")
        sys.exit(1)


def create_server(port: int) -> HTTPServer:
    httpserver = HTTPServer(port=port)
    httpserver.expect_request(
        "/v3/auth/verify/", method="GET", headers={"Authorization": "SPTKN example_api_token"}
    ).respond_with_json({"username": "user", "token_type": "api"}, status=200)

    httpserver.expect_request("/v3/scans_async/", method="POST").respond_with_json(
        load_json("data/scan_async_response.json"), status=201
    )
    httpserver.expect_request("/v3/scans/VzhZjnCQT5SaGfd6EQG_Gg", method="GET").respond_with_json(
        load_json("data/scan_response.json"), status=200
    )

    httpserver.expect_request("/v2/opa/", method="PUT").respond_with_json({}, status=200)

    httpserver.expect_request("/v3/configuration/", method="PATCH").respond_with_json({}, status=200)

    httpserver.expect_request("/v3/configuration/", method="GET").respond_with_json(
        load_json("data/get_config_response.json"), status=200
    )

    httpserver.expect_request(
        "/v2/ai/query/modules/", method="GET", query_string={"query": "Create a new EC2 instance", "num_results": "3"}
    ).respond_with_json(load_json("data/suggest_response.json"), status=200)

    me_response = {
        "id": "user123456789",
        "username": "johndoe",
        "email": "john.doe@example.com",
        "is_instance_admin": False,
        "first_name": "John",
        "last_name": "Doe",
        "is_email_activated": True,
        "organizations": [
            {
                "id": "org123456789",
                "users": ["user123", "user1235", "user125"],
                "role": "org_admin",
                "free_trial_used": False,
                "subscription": {
                    "id": "sub123456789",
                    "organization": "org123456789",
                    "user": None,
                    "plan_type": "team",
                    "service_type": "saas",
                    "recurrence": "perpetual",
                    "status": "active",
                    "created_date": "2025-09-27T11:24:46.092772Z",
                    "start_date": "2025-09-27T11:24:46.092772Z",
                    "end_date": "2050-01-01T00:00:00Z",
                    "cost": None,
                    "max_users": 50,
                    "custom_policies_enabled": True,
                    "invoice_id": None,
                    "full_name": None,
                    "company": None,
                    "address": None,
                    "postcode": None,
                    "city": None,
                    "state": None,
                    "country": None,
                    "phone_num": None,
                    "vat_id": None,
                    "payment_method": None,
                    "discount_code": None,
                    "price": None,
                    "tax_rate": None,
                    "discount_rate": None,
                    "is_vat_valid": True,
                },
                "pending_subscriptions": [],
                "allow_subscription_management": True,
                "projects": ["proj123456789"],
                "name": "john's personal organization",
                "description": "This is your personal organization",
            }
        ],
        "default_organization": "org123456789",
        "company": None,
        "phone_number": None,
        "date_joined": "2025-08-25T12:26:58.416313Z",
    }
    httpserver.expect_request("/v2/users/me/", method="GET").respond_with_json(me_response, status=200)

    return httpserver


if __name__ == "__main__":
    server = create_server(get_port())
    server.start()  # type: ignore

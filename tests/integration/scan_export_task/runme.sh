#!/bin/bash

set -euo pipefail

echo "Running scan_export_task ..."
spotter_executable="$1"
payload_file="$(mktemp).json"

$spotter_executable scan --export-payload "$payload_file" task.yml
test $? = 0

diff <(jq '.environment.python_version = "ANY" |
           .environment.ansible_version.ansible_core = null |
           .environment.ansible_version.ansible_base = null |
           .environment.ansible_version.ansible = null |
           .environment.ansible_config = {} |
           .environment.installed_collections = [] |
           .environment.cli_scan_args.version = "ANY" |
           .tasks[].task_id = "ANY" |
           .tasks[].play_id = "ANY" |
           .tasks[].spotter_metadata.file = "ANY"' "$payload_file") "payload.json"
test $? = 0
rm -rf "$payload_file"
echo "Done!"

#!/bin/bash

set -euo pipefail

function file_contains () {
    grep -q "$1" "$2"
}

echo "Running scan_export_collection ..."
spotter_executable="$1"
payload_file="$(mktemp).json"

$spotter_executable scan --export-payload "$payload_file" collection/
test $? = 0

file_contains "playbooks/playbook.yml" "$payload_file"
file_contains "roles/role/handlers/main.yml" "$payload_file"
file_contains "roles/role/tasks/folder1/test.yml" "$payload_file"
file_contains "roles/role/tasks/folder2/test1.yml" "$payload_file"
file_contains "roles/role/tasks/folder2/test2.yml" "$payload_file"
file_contains "roles/role/tasks/folder3/subfolder1/test1.yml" "$payload_file"
file_contains "roles/role/tasks/folder3/subfolder1/test2.yml" "$payload_file"
file_contains "roles/role/tasks/folder3/subfolder1/subsubfolder1/test.yml" "$payload_file"
file_contains "roles/role/tasks/folder3/subfolder2/test.yml" "$payload_file"
file_contains "roles/role/tasks/folder3/test.yml" "$payload_file"
file_contains "roles/role/tasks/main.yml" "$payload_file"
file_contains "roles/role/tasks/test.yml" "$payload_file"
file_contains "tests/integration/targets/role/handlers/main.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder1/test.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder2/test1.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder2/test2.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder3/subfolder1/test1.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder3/subfolder1/test2.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder3/subfolder1/subsubfolder1/test.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder3/subfolder2/test.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/folder3/test.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/main.yml" "$payload_file"
file_contains "tests/integration/targets/role/tasks/test.yml" "$payload_file"
file_contains "playbook.yml" "$payload_file"
file_contains "task.yml" "$payload_file"

diff <(jq '.environment.python_version = "ANY" |
           .environment.ansible_version.ansible_core = null |
           .environment.ansible_version.ansible_base = null |
           .environment.ansible_version.ansible = null |
           .environment.ansible_config = {} |
           .environment.installed_collections = [] |
           .environment.cli_scan_args.version = "ANY" |
           .tasks[].task_id = "ANY" |
           .tasks[].play_id = "ANY" |
           .tasks[].spotter_metadata.file = "ANY" |
           .playbooks[].playbook_id = "ANY" |
           .playbooks[].plays[].play_id = "ANY" |
           .playbooks[].plays[].spotter_metadata = "ANY"' "$payload_file") "payload.json"
test $? = 0
rm -rf "$payload_file"
echo "Done!"

#!/bin/bash

set -euo pipefail

echo "Running scan_export_playbook_flexible ..."
spotter_executable="$1"
payload_file="$(mktemp).json"

$spotter_executable scan --project-id example_id_123 \
                         --config config.yml \
                         --ansible-version 2.16 \
                         --rewrite \
                         --display-level warning \
                         --profile full \
                         --no-docs-url \
                         --no-scan-url \
                         --no-progress \
                         --format json \
                         --output scan.json \
                         --export-payload "$payload_file" .
test $? = 0

diff <(jq '.environment.python_version = "ANY" |
           .environment.ansible_version.ansible_base = null |
           .environment.ansible_version.ansible = null |
           .environment.ansible_config = {} |
           .environment.installed_collections = [] |
           .environment.cli_scan_args.version = "ANY" |
           .tasks[].task_id = "ANY" |
           .tasks[].play_id = "ANY" |
           .tasks[].spotter_metadata.file = "ANY" |
           .playbooks[].playbook_id = "ANY" |
           .playbooks[].plays[].play_id = "ANY" |
           .playbooks[].plays[].spotter_metadata = "ANY"' "$payload_file") "payload.json"
test $? = 0
rm -rf "$payload_file"
echo "Done!"
